
# Install bash shell
#
class shell::bash {

    package { "bash":
        ensure => installed,
    }

}


# Install tcsh shell
#
class shell::tcsh {

    package { "tcsh":
        ensure => installed,
    }

}


# Install zsh shell
#
class shell::zsh {

    package { "zsh":
        ensure => installed,
    }

}
