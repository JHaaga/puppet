
# Install netcat
#
class netcat {

    if $::operatingsystem != "OpenBSD" {
        package { "netcat":
            name   => $::operatingsystem ? {
                "ubuntu" => "netcat",
                "fedora" => $::operatingsystemrelease ? {
                    /^1[0-7]/ => "nc",
                    default   => "nmap-ncat",
                },
                default  => "nc",
            },
            ensure => present,
        }
    }

}
