# Install Clarified Analyzer.
#
class clarified::analyzer {

    include ia32libs

    if !$clarified_analyzer_package {
        if $clarified_analyzer_package_latest {
            $clarified_analyzer_package = $clarified_analyzer_package_latest
        } else {
            fail("Must define \$clarified_analyzer_package or \$clarified_analyzer_package_latest")
        }
    }

    file { "/usr/local/src/clarified-analyzer-linux-i686.sh":
        ensure => present,
        mode   => "0755",
        owner  => root,
        group  => root,
        source => "puppet:///files/packages/${clarified_analyzer_package}",
        before => Exec["/usr/local/src/clarified-analyzer-linux-i686.sh"],
    }
    exec { "rm -f /usr/local/clarified-analyzer":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        onlyif      => "test -h /usr/local/clarified-analyzer",
        subscribe   => File["/usr/local/src/clarified-analyzer-linux-i686.sh"],
        before      => Exec["/usr/local/src/clarified-analyzer-linux-i686.sh"],
        refreshonly => true,
    }
    exec { "/usr/local/src/clarified-analyzer-linux-i686.sh":
        creates => "/usr/local/clarified-analyzer",
    }

}


# Install Clarified Recorder.
#
class clarified::recorder {

    if !$clarified_recorder_package {
        if $clarified_recorder_package_latest {
            $clarified_recorder_package = $clarified_recorder_package_latest
        } else {
            fail("Must define \$clarified_recorder_package or \$clarified_recorder_package_latest")
        }
    }

    if $recorder_datadir {
        file { $recorder_datadir:
            ensure => directory,
            mode   => "0700",
            owner  => root,
            group  => root,
        }

        file { "/var/lib/recorder":
            ensure  => link,
            target  => $recorder_datadir,
            require => File[$recorder_datadir],
        }
    } else {
        file { "/var/lib/recorder":
            ensure => directory,
            mode   => "0700",
            owner  => root,
            group  => root,
        }
    }

    file { [ "/etc/clarified",
             "/etc/clarified/probe.d",
             "/etc/clarified/remote.d", ]:
        ensure => directory,
        mode   => "0644",
        owner  => root,
        group  => root,
        before => Exec["/usr/local/src/clarified-recorder-linux.sh"],
    }

    File["/etc/clarified/probe.d", "/etc/clarified/remote.d"] {
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
    }

    file { "/usr/local/src/clarified-recorder-linux.sh":
        ensure => present,
        mode   => "0755",
        owner  => root,
        group  => root,
        source => "puppet:///files/packages/${clarified_recorder_package}",
        before => Exec["/usr/local/src/clarified-recorder-linux.sh"],
    }
    exec { "rm -f /usr/local/probe":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        onlyif      => "test -h /usr/local/probe",
        subscribe   => File["/usr/local/src/clarified-recorder-linux.sh"],
        before      => Exec["/usr/local/src/clarified-recorder-linux.sh"],
        refreshonly => true,
    }
    exec { "/usr/local/src/clarified-recorder-linux.sh":
        creates => "/usr/local/probe",
        notify  => Service["clarified-probe"],
    }

    exec { "clarified-functions":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        cwd         => "/usr/local/probe",
        command     => "sed s:@PREFIX@:/usr/local/probe: clarified-functions.in > /etc/clarified/clarified-functions",
        subscribe   => Exec["/usr/local/src/clarified-recorder-linux.sh"],
        before      => Service["clarified-probe"],
        refreshonly => true,
    }

    file { "/etc/init.d/clarified-probe":
        ensure  => present,
        mode    => "0755",
        owner   => root,
        group   => root,
        source  => "/usr/local/probe/probe-init.sh",
        require => Exec["/usr/local/src/clarified-recorder-linux.sh"],
        before  => Service["clarified-probe"],
    }

    service { "clarified-probe":
        ensure     => running,
        enable     => true,
        hasrestart => true,
        status     => "pgrep -f /usr/local/probe",
    }

}


# Configure probe.
#
# === Parameters
#
#   $name:
#       Probe name.
#   $interface:
#       Capture interface. Defaults to probe name.
#   $snaplen:
#       Snaplen. Defaults to 65535.
#   $keeptime:
#       Amount of data to keep. Defaults to 100GB.
#   $blocksize:
#       Storage block size. Defaults to 1GB.
#   $filter:
#       Optional filter expression.
#   $remoteport:
#       Remote port. Defaults to 10000.
#   $collab:
#       List of collabs for authentication.
#   $probe:
#       Enable probe. Defaults to true.
#   $remote:
#       Enable remote. Defaults to true.
#
# === Sample usage
#
# clarified::probe { "eth0":
#     keeptime  => "500GB",
#     blocksize => "10GB",
#     filter    => "host 192.168.1.1",
#     collab    => [ "collabname:PageName" ],
# }
#
define clarified::probe($interface="", $snaplen="65535", $keeptime="100GB",
                        $blocksize="1GB", $filter="", $remoteport="10000",
                        $collab=[], $probeopt="", $remoteopt="",
                        $probe=true, $remote=true) {

    if $interface {
        $interface_real = $interface
    } else {
        $interface_real = $name
    }

    file { "/etc/clarified/probe.d/${name}":
        ensure  => $probe ? {
            true  => present,
            false => absent,
        },
        mode    => "0755",
        owner   => root,
        group   => root,
        content => template("clarified/probe.erb"),
        require => File["/etc/clarified/probe.d"],
        notify  => Service["clarified-probe"],
    }

    file { "/etc/clarified/remote.d/${name}":
        ensure  => $remote ? {
            true  => present,
            false => absent,
        },
        mode    => "0755",
        owner   => root,
        group   => root,
        content => template("clarified/remote.erb"),
        require => File["/etc/clarified/remote.d"],
        notify  => Service["clarified-probe"],
    }

    file { "/var/lib/recorder/${name}":
        ensure  => directory,
        mode    => "0700",
        owner   => root,
        group   => root,
        require => File["/var/lib/recorder"],
        before  => Service["clarified-probe"],
    }

}
