
# Install MiniDLNA
#
# === Parameters
#
#   $name:
#       Name that minidlna sends to clients. Defaults to
#       MiniDLNA.
#   $audiodir:
#       Directory containing audio files.
#   $videodir:
#       Directory containing video files.
#   $photodir:
#       Directory containing image files.
#
class minidlna($name="", $audiodir="", $videodir="", $photodir="") {

    package { "minidlna":
        ensure => installed,
    }

    file { "minidlna.conf":
        ensure  => present,
        path    => "/etc/minidlna.conf",
        content => template("minidlna/minidlna.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["minidlna"],
        notify  => Service["minidlna"],
    }

    service { "minidlna":
        ensure => running,
        enable => true,
    }

}
