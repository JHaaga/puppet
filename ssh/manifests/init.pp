# Export and collect public host keys.
#
class ssh::known_hosts {

    file { "/etc/ssh/ssh_known_hosts":
        ensure => present,
        mode   => "0644",
        owner  => root,
        group  => $::operatingsystem ? {
            OpenBSD => wheel,
            default => root,
        },
    }

    $aliases = merge(inline_template("<%= @homename.split('.')[0] %>"),
                     $::ipaddress,
                     $::ipaddress6,
                     $::ec2_public_ipv4)

    @@sshkey { $homename:
        ensure       => present,
        type         => rsa,
        key          => $sshrsakey,
        host_aliases => $aliases,
        require      => File["/etc/ssh/ssh_known_hosts"],
    }

    Sshkey <<| |>>

}


# Install SSH host keys.
#
class ssh::hostkeys {

    tag("bootstrap")

    file { "/etc/ssh/ssh_host_dsa_key":
        ensure => present,
        source => "puppet:///private/ssh_host_dsa_key",
        mode   => "0600",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }
    file { "/etc/ssh/ssh_host_dsa_key.pub":
        ensure => present,
        source => "puppet:///private/ssh_host_dsa_key.pub",
        mode   => "0644",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }

    file { "/etc/ssh/ssh_host_rsa_key":
        ensure => present,
        source => "puppet:///private/ssh_host_rsa_key",
        mode   => "0600",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }
    file { "/etc/ssh/ssh_host_rsa_key.pub":
        ensure => present,
        source => "puppet:///private/ssh_host_rsa_key.pub",
        mode   => "0644",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }

    file { "/etc/ssh/ssh_host_key":
        ensure => present,
        source => "puppet:///private/ssh_host_key",
        mode   => "0600",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }
    file { "/etc/ssh/ssh_host_key.pub":
        ensure => present,
        source => "puppet:///private/ssh_host_key.pub",
        mode   => "0644",
        owner  => root,
        group  => $::operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }

}


# Enable SSH server.
#
class ssh::server {

    if $::operatingsystem != "OpenBSD" {
        package { "openssh-server":
            ensure => installed,
            before => Service["sshd"],
        }
    }

    service { "sshd":
        name   => $::operatingsystem ? {
            "ubuntu" => "ssh",
            default  => "sshd",
        },
        ensure => running,
        enable => true,
    }

}


# Disable SSH server.
#
class ssh::disable inherits ssh::server {

    case $::operatingsystem {
        "ubuntu": {
            file { "/etc/init/ssh.conf":
                ensure => present,
                mode   => "0644",
                owner  => root,
                group  => root,
                source => "puppet:///modules/ssh/ssh.disabled.conf",
                before => Service["sshd"],
            }
        }
    }

    Service["sshd"] {
        ensure => stopped,
        enable => false,
    }

}


# Set AllowGroups in sshd_config.
#
# === Global variables
#
#   $ssh_allowgroups:
#       Array of groups, root or wheel is always allowed.
#
class ssh::allowgroups {

    include ssh::server

    $root_group = $::operatingsystem ? {
        "openbsd" => "wheel",
        default   => "root",
    }

    if $ssh_allowgroups {
        $ssh_allowgroups_real = inline_template("${root_group} <%= @ssh_allowgroups.join(' ') %>")
    } else {
        $ssh_allowgroups_real = $root_group
    }

    exec { "ssh-allowgroups-set":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
        cwd     => "/etc/ssh",
        command => "echo 'AllowGroups ${ssh_allowgroups_real}' >> sshd_config",
        unless  => "grep -q '^[^#]*AllowGroups' sshd_config",
        before  => Exec["ssh-allowgroups-sub"],
        notify  => Service["sshd"],
    }
    exec { "ssh-allowgroups-sub":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
        cwd     => "/etc/ssh",
        command => "ruby -pi -e 'sub(/(AllowGroups).*/, \"\\\\1 ${ssh_allowgroups_real}\")' sshd_config",
        unless  => "grep -q '^[^#]*AllowGroups ${ssh_allowgroups_real}' sshd_config",
        notify  => Service["sshd"],
    }

}
