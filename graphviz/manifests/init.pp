# Install graphviz.
#
class graphviz {

    package { "graphviz":
        ensure => installed,
    }

}

# Install python bindings for graphviz.
#
class graphviz::python inherits graphviz {

    package { "graphviz-python":
        name   => $::operatingsystem ? {
            debian  => "libgv-python",
            ubuntu  => "libgv-python",
            default => "graphviz-python",
        },
        ensure => installed,
    }

}
