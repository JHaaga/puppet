
# Common pam prequisites
#
class pam::common {

    case $::operatingsystem {
        "centos","redhat","fedora": {
            package { "authconfig":
                ensure => installed,
            }
        }
        "ubuntu": {
            package { "libpam-runtime":
                ensure => installed,
            }
            exec { "pam-auth-update":
                path        => "/bin:/usr/bin:/sbin:/usr/sbin",
                refreshonly => true,
                require     => Package["libpam-runtime"],
            }
        }
    }

}


# Enable pam_mkhomedir module
#
class pam::mkhomedir {

    include pam::common

    case $::operatingsystem {
        "centos","redhat","fedora": {
            exec { "authconfig --enablemkhomedir --update":
                path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                unless  => "egrep '^USEMKHOMEDIR=yes\$' /etc/sysconfig/authconfig",
                require => Package["authconfig"],
            }
        }
        "ubuntu": {
            file { "/usr/share/pam-configs/pam_mkhomedir":
                source  => "puppet:///modules/pam/pam_mkhomedir",
                mode    => "0644",
                owner   => "root",
                group   => "root",
                require => Package["libpam-runtime"],
                notify  => Exec["pam-auth-update"],
            }
        }
    }

}
