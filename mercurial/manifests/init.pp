# Install Mercurial.
#
class mercurial {

    package { "mercurial":
        ensure => installed,
    }

}

class mercurial::client {

    warning("mercurial::client is deprecated, include mercurial instead")
    include mercurial

}


# Clone repository.
#
# === Parameters
#
#   $name:
#       Destination directory.
#   $source:
#       Source URL.
#   $ensure:
#       Revision. Defaults to tip.
#
define mercurial::clone($source, $ensure="tip") {

    exec { "hg-clone-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "hg -y clone -r ${ensure} ${source} ${name}",
        creates => $name,
        require => Package["mercurial"],
    }

    exec { "hg-pull-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        cwd     => $name,
        command => "hg -y pull -u -r ${ensure}",
        onlyif  => $ensure ? {
            "tip"   => "hg -y in",
            default => "test $(hg -y id -i) != ${ensure}",
        },
        require => Exec["hg-clone-${name}"],
    }

}
