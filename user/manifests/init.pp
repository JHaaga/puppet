# Class which contains all system users that have fixed UID's
#
class user::system {

    file { "/var/empty":
        ensure => directory,
        mode   => "0755",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

    if $::operatingsystem == "ubuntu" {
        file { "/sbin/nologin":
            ensure => link,
            target => "/usr/sbin/nologin",
            owner  => "root",
            group  => "root",
        }
    }

    User {
        require => File["/var/empty"],
    }

    @group { "httpsd":
        ensure => present,
        gid    => 800,
    }
    @user { "httpsd":
        ensure  => present,
        uid     => 800,
        gid     => 800,
        comment => "Service HTTPS",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["httpsd"],
    }

    @group { "smbguest":
        ensure => present,
        gid    => 801,
    }
    @user { "smbguest":
        ensure  => present,
        uid     => 801,
        gid     => 801,
        comment => "Service AWIMS",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["smbguest"],
    }

    # ejabber server daemon
    @group { "ejabberd":
        ensure => present,
        gid    => 802,
    }
    @user { "ejabberd":
        ensure  => present,
        uid     => 802,
        gid     => 802,
        comment => "Service Jabber",
        home    => "/var/lib/ejabberd",
        shell   => $::operatingsystem ? {
            "ubuntu" => "/bin/sh",
            default  => "/sbin/nologin",
        },
        require => Group["ejabberd"],
    }

    # Locate database owner
    @group { "locate":
        ensure => present,
        gid    => 804,
    }
    @user { "locate":
        ensure  => present,
        uid     => 804,
        gid     => 804,
        comment => "Service Locate",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["locate"],
    }

    # License server daemon
    @group { "licensed":
        ensure => present,
        gid    => 805,
    }
    @user { "licensed":
        ensure  => present,
        uid     => 805,
        gid     => 805,
        comment => "Service Licensed",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["licensed"],
    }

    # VMware Server 1.x daemon
    @group { "vmwared":
        ensure => present,
        gid    => 806,
    }
    @user { "vmwared":
        ensure  => present,
        uid     => 806,
        gid     => 806,
        comment => "Service VMware",
        home    => "/home/vmwared",
        shell   => "/sbin/nologin",
        require => Group["vmwared"],
    }

    # Samba domain computer account
    @group { "smbhost":
        ensure => present,
        gid    => 807,
    }
    @user { "smbhost":
        ensure  => present,
        uid     => 807,
        gid     => 807,
        comment => "Samba Host",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["smbhost"],
    }

    # BackupPC server daemon
    @group { "backuppc":
        ensure => present,
        gid    => 808,
    }
    @user { "backuppc":
        ensure  => present,
        uid     => 808,
        gid     => 808,
        comment => "Service BackupPC",
        home    => "/var/lib/BackupPC",
        shell   => "/sbin/nologin",
        require => Group["backuppc"],
    }

    # SunRay Windows Connector
    @group { "srwc":
        ensure => present,
        gid    => 809,
    }

    # Samba Domain Admins group
    @group { "smbadmin":
        ensure => present,
        gid    => 810,
    }

    # MythTV server daemon
    @group { "mythtv":
        ensure => present,
        gid    => 811,
    }
    @user { "mythtv":
        ensure  => present,
        uid     => 811,
        gid     => 811,
        comment => "Service MythTV",
        home    => "/var/lib/mythtv",
        shell   => "/sbin/nologin",
        require => Group["mythtv"],
    }

    # Collab Helper Account & Group
    @group { "collab":
        ensure => present,
        gid    => 812,
    }
    @user { "collab":
        ensure  => present,
        uid     => 812,
        gid     => 812,
        comment => "Service Collab",
        home    => "/var/lib/collab",
        shell   => "/sbin/nologin",
        require => Group["collab"],
    }

    # AbuseHelper
    @group { "abusehel":
        ensure => present,
        gid    => 813,
    }
    @user { "abusehel":
        ensure  => present,
        uid     => 813,
        gid     => 813,
        comment => "Service AbuseHelper",
        home    => "/var/lib/ah2",
        shell   => "/sbin/nologin",
        require => Group["abusehel"],
    }

    # VSRoom
    @group { "vsroom":
        ensure => present,
        gid    => 814,
    }
    @user { "vsroom":
        uid     => 814,
        gid     => 814,
        comment => "Service VSRoom",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["vsroom"],
    }

    # QEMU for KVM
    @group { "qemu":
        ensure => present,
        gid    => 815,
    }
    @user { "qemu":
        uid     => 815,
        gid     => 815,
        comment => "Service QEMU",
        home    => "/var/lib/qemu",
        shell   => "/sbin/nologin",
        require => Group["qemu"],
    }

    # Etherpad
    @group { "etherpad":
        ensure => present,
        gid    => 816,
    }
    @user { "etherpad":
        uid     => 816,
        gid     => 816,
        comment => "Service Etherpad",
        home    => "/var/lib/etherpad",
        shell   => "/sbin/nologin",
        require => Group["etherpad"],
    }

    # AbuseSA
    @group { "abusesa":
        ensure => present,
        gid    => 817,
    }
    @user { "abusesa":
        ensure  => present,
        uid     => 817,
        gid     => 817,
        comment => "Service AbuseSA",
        home    => "/var/lib/abusesa",
        shell   => "/sbin/nologin",
        require => Group["abusesa"],
    }

    # TeamSpeak
    @group { "teamspeak":
        ensure => present,
        gid    => 818,
    }
    @user { "teamspeak":
        ensure  => present,
        uid     => 818,
        gid     => 818,
        comment => "Service TeamSpeak",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["teamspeak"],
    }

    # Murmur (Mumble server)
    @group { "murmur":
        ensure => present,
        gid    => 819,
    }
    @user { "murmur":
        ensure  => present,
        uid     => 819,
        gid     => 819,
        comment => "Service Murmur",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["murmur"],
    }

    # Mirrors
    @group { "mirror":
        ensure => present,
        gid    => 820,
    }
    @user { "mirror":
        ensure  => present,
        uid     => 820,
        gid     => 820,
        comment => "Service Mirror",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["mirror"],
    }

    # SNMP trapd
    @group { "snmptrapd":
        ensure => present,
        gid    => 821,
    }
    @user { "snmptrapd":
        ensure  => present,
        uid     => 821,
        gid     => 821,
        comment => "Service SNMPTrapd",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["snmptrapd"],
    }

    # Webalizer
    @group { "webalizer":
        ensure => present,
        gid    => 822,
    }
    @user { "webalizer":
        ensure  => present,
        uid     => 822,
        gid     => 822,
        comment => "Service Webalizer",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["webalizer"],
    }

    # Apache Solr
    @group { "solr":
        ensure => present,
        gid    => 823,
    }
    @user { "solr":
        ensure  => present,
        uid     => 823,
        gid     => 823,
        comment => "Service Solr",
        home    => "/var/empty",
        shell   => "/sbin/nologin",
        require => Group["solr"],
    }

}


# Add local user account.
#
define user::newuser($uid, $gid, $comment, $home, $shell, $groups=undef, $requiregroups=undef) {

    user { $name:
        ensure  => present,
        uid     => $uid,
        gid     => $gid,
        comment => $comment,
        home    => $home,
        shell   => $shell,
        groups  => $groups,
        require => $requiregroups,
        notify  => $::operatingsystem ? {
            OpenBSD => [ Exec["user-mod-${name}"],
                         Exec["user-home-${name}"], ],
            default => undef,
        }
    }

    exec { "user-mod-${name}":
        command     => "usermod -L ldap ${name}",
        path        => "/sbin:/usr/sbin:/bin:/usr/bin",
        refreshonly => true,
        require     => File["/etc/login.conf"],
    }

    exec { "user-home-${name}":
        command     => "/bin/sh -c 'umask 077; mkdir -p ${home} && tar cf - . | tar xf - -C ${home} && chown -R ${uid}:${gid} ${home}'",
        cwd         => "/etc/skel",
        path        => "/sbin:/usr/sbin:/bin:/usr/bin",
        creates     => $home,
        refreshonly => true,
    }

}
