# Install logwatch.
#
# === Global variables
#
#   $logwatch_mailto
#       Logwatch mail recipient.
#
class logwatch {

    case $::kernel {
        "linux": {
            package { "logwatch":
                ensure => installed,
            }
        }
        default: {
            fail("logwatch not supported on ${::kernel}")
        }
    }

    if $logwatch_mailto {
        file { "/etc/logwatch/conf/logwatch.conf":
            ensure  => present,
            mode    => "0644",
            owner   => "root",
            group   => "root",
            content => "MailTo = ${logwatch_mailto}\n",
            require => Package["logwatch"],
        }
    }

}
