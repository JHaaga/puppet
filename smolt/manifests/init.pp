
# Install smolt paackage.
#
class smolt::package {

    package { "smolt":
        ensure => installed,
    }

}


# Enable smolt monthly updates.
#
class smolt::client inherits smolt::package {

    service { "smolt":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package["smolt"],
    }

}
