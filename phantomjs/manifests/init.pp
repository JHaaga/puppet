# Install PhantomJS.
#
class phantomjs {

    if !$phantomjs_package {
        case $::architecture {
            "amd64","x86_64": {
                if $phantomjs64_package_latest {
                    $phantomjs_package = $phantomjs64_package_latest
                } else {
                    fail("Must define \$phantomjs_package or \$phantomjs64_package_latest")
                }
            }
            default: {
                if $phantomjs32_package_latest {
                    $phantomjs_package = $phantomjs32_package_latest
                } else {
                    fail("Must define \$phantomjs_package or \$phantomjs32_package_latest")
                }
            }
        }
    }

    file { "/usr/local/src/phantomjs.tar.bz2":
        ensure => present,
        source => "puppet:///files/packages/${phantomjs_package}",
        mode   => "0644",
        owner  => "root",
        group  => "root",
    }
    util::extract::tar { "/usr/local/lib/phantomjs":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/phantomjs.tar.bz2",
        require => File["/usr/local/src/phantomjs.tar.bz2"],
    }
    file { "/usr/local/bin/phantomjs":
        ensure  => link,
        target  => "/usr/local/lib/phantomjs/bin/phantomjs",
        require => Util::Extract::Tar["/usr/local/lib/phantomjs"],
    }

}
