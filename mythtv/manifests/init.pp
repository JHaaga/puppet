
# Install MythTV backend service.
#
# === Global variables
#
#   $mythtv_dbhost:
#       Database server name (defaults to localhost).
#
#   $mythtv_dbname:
#       Database name (defaults to mythconverg).
#
#   $mythtv_dbuser:
#       Database user name (defaults to mythtv).
#
#   $mythtv_dbpass:
#       Database password.
#
class mythtv::backend {

    if ! $mythtv_dbhost {
        $mythtv_dbhost = "localhost"
    }
    if ! $mythtv_dbuser {
        $mythtv_dbuser = "mythtv"
    }
    if ! $mythtv_dbname {
        $mythtv_dbname = "mythconverg"
    }

    include user::system
    realize(User["mythtv"], Group["mythtv"])

    include mysql::client

    package { [
          "mythtv-backend",
          "mythtv-setup",
          "mythtv-docs",
          "perl-DBD-MySQL",
        ]:
        ensure  => installed,
        require => [ User["mythtv"], Group["mythtv"], ],
    }

    file { "/etc/mythtv/mysql.txt":
        ensure  => present,
        content => template("mythtv/mysql.txt.erb"),
        mode    => "0640",
        owner   => root,
        group   => mythtv,
        notify  => Service["mythbackend"],
        require => Package["mythtv-backend"],
    }

    file { "/etc/mythtv/config.xml":
        ensure  => present,
        content => template("mythtv/config.xml.erb"),
        mode    => "0640",
        owner   => root,
        group   => mythtv,
        notify  => Service["mythbackend"],
        require => Package["mythtv-backend"],
    }

    file { "/etc/mythtv/.mythtv":
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => Package["mythtv-backend"],
    }
    file { "/etc/mythtv/.mythtv/mysql.txt":
        ensure  => link,
        target  => "../mysql.txt",
        owner   => "root",
        group   => "root",
        require => [
          File["/etc/mythtv/mysql.txt"],
          File["/etc/mythtv/.mythtv"],
        ],
        before  => Service["mythbackend"],
    }
    file { "/etc/mythtv/.mythtv/config.xml":
        ensure  => link,
        target  => "../config.xml",
        owner   => "root",
        group   => "root",
        require => [
          File["/etc/mythtv/config.xml"],
          File["/etc/mythtv/.mythtv"],
        ],
        before  => Service["mythbackend"],
    }


    file { "/etc/sysconfig/mythbackend":
        ensure  => present,
        source  => "puppet:///modules/mythtv/mythbackend",
        mode    => "0644",
        owner   => root,
        group   => root,
        notify  => Service["mythbackend"],
        require => Package["mythtv-backend"],
    }

    file { "/root/.mythtv":
        ensure  => link,
        target  => "/etc/mythtv",
        force   => true,
        require => Package["mythtv-backend"],
    }

    service { "mythbackend":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => [ Package["mythtv-backend"], Package["mythtv-setup"], ],
    }

    file { "/etc/cron.daily/mythorphans":
        ensure  => present,
        source  => "puppet:///modules/mythtv/mythorphans",
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => File["/usr/local/bin/myth.find_orphans.pl"],
    }
    file { "/usr/local/bin/myth.find_orphans.pl":
        ensure => present,
        source => "puppet:///modules/mythtv/myth.find_orphans.pl",
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }

}


# Install MythTV daily cron jobs.
#
# This class should be included only to one host.
#
class mythtv::cron {

    require mythtv::backend

    package { [ "perl-MythTV", "perl-Net-UPnP" ]:
        ensure => installed,
    }

    file { "/etc/cron.daily/mythfilldatabase":
        ensure => present,
        source => "puppet:///modules/mythtv/mythfilldatabase",
        mode   => "0755",
        owner  => root,
        group  => root,
    }

    file { "/etc/cron.daily/mythoptimize":
        ensure => present,
        source => "puppet:///modules/mythtv/mythoptimize",
        mode   => "0755",
        owner  => root,
        group  => root,
    }

}


# Install MythTV web frontend.
#
class mythtv::mythweb {

    package { [ "mythweb", "mythweather" ]:
        ensure => installed,
    }

    apache::configfile { "mythweb.conf":
        content => template("mythtv/mythweb.conf.erb"),
        http    => false,
        require => Package["mythweb"],
    }

    if $::operatingsystem == "Fedora" {
        if $::operatingsystemrelease < 13 {
            $context = "httpd_sys_content_rw_t"
        } else {
            $context = "httpd_sys_rw_content_t"
        }
    }
    selinux::manage_fcontext { "/usr/share/mythweb/data(/.*)?":
        type   => $context,
        before => File["/usr/share/mythweb/data"],
    }

    if ! $apache::sslserver::group {
        fail("\$apache::sslserver::group not defined, include apache::sslserver class before mythtv::mythweb class")
    }

    file { [
          "/usr/share/mythweb/data",
          "/usr/share/mythweb/data/tv_icons",
          "/usr/share/mythweb/data/cache",
        ]:
        ensure  => directory,
        mode    => "0770",
        owner   => "root",
        group   => $apache::sslserver::group,
        seltype => "httpd_sys_rw_content_t",
        require => Package["mythweb"],
    }

}
