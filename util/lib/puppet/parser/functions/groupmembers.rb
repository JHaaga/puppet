
require 'etc'

module Puppet::Parser::Functions
    newfunction(:groupmembers, :type => :rvalue) do |args|
        Etc.getgrnam(args[0])['mem']       
    end
end

