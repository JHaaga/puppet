# Install AbuseSA.
#
# === Global variables
#
#   $abusesa_datadir
#       AbuseSA home directory. Defaults to /var/lib/abusesa.
#
#   $abusesa_botnets
#       Array of botnet paths to start at boot.
#
class abusesa {

    case $::operatingsystem {
        "centos","redhat": {
            case $::operatingsystemrelease {
                /^5/: {
                    package { "python26":
                        ensure => installed,
                    }
                    Python::Setup::Install["/usr/local/src/abusesa"] {
                        python  => "python2.6",
                        require => Package["python26"],
                    }
                }
            }
        }
    }

    if !$abusesa_package {
        if $abusesa_package_latest {
            $abusesa_package = $abusesa_package_latest
        } else {
            fail("Must define \$abusesa_package or \$abusesa_package_latest")
        }
    }

    include user::system
    realize(User["abusesa"], Group["abusesa"])

    if $abusesa_datadir {
        file { $abusesa_datadir:
            ensure  => directory,
            mode    => "2770",
            owner   => "abusesa",
            group   => "abusesa",
            require => User["abusesa"],
        }
        file { "/var/lib/abusesa":
            ensure  => link,
            target  => $abusesa_datadir,
            require => File[$abusesa_datadir],
        }
    } else {
        file { "/var/lib/abusesa":
            ensure  => directory,
            mode    => "2770",
            owner   => "abusesa",
            group   => "abusesa",
            require => User["abusesa"],
        }
    }

    file { "/usr/local/src/abusesa.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source => "puppet:///files/packages/${abusesa_package}",
    }
    util::extract::tar { "/usr/local/src/abusesa":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/abusesa.tar.gz",
        require => File["/usr/local/src/abusesa.tar.gz"],
        before  => Python::Setup::Install["/usr/local/src/abusesa"],
    }
    python::setup::install { "/usr/local/src/abusesa": }

    if $abusesa_botnets {
        $abusehelper_botnets = $abusesa_botnets
        $abusehelper_user = "abusesa"
        include abusehelper::init
    }

}


# Install AbuseSA Search.
#
class abusesa::search {

    if !$abusesa_search_package {
        if $abusesa_search_package_latest {
            $abusesa_search_package = $abusesa_search_package_latest
        } else {
            fail("Must define \$abusesa_search_package or \$abusesa_search_package_latest")
        }
    }

    file { "/usr/local/src/abusesa-search.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///files/packages/${abusesa_search_package}",
    }

    util::extract::tar { "/usr/local/src/abusesa-search":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/abusesa-search.tar.gz",
        require => File["/usr/local/src/abusesa-search.tar.gz"],
        before  => Python::Setup::Install["/usr/local/src/abusesa-search"],
    }

    python::setup::install { "/usr/local/src/abusesa-search": }

    package { [ "python-BeautifulSoup",
                "python-lxml", ]:
        ensure => installed,
    }

    file { "/srv/solr/cores/generic":
        ensure  => directory,
        mode    => "0660",
        owner   => "solr",
        group   => "solr",
        source  => "/usr/local/src/abusesa-search/config/solr/cores/generic",
        recurse => true,
        purge   => true,
        force   => true,
        require => [
            File["/srv/solr/cores"],
            Util::Extract::Tar["/usr/local/src/abusesa-search"],
        ],
    }
    file { "/srv/solr/cores/lib/solr-analysis-clarified.jar":
        ensure  => present,
        mode    => "0660",
        owner   => "solr",
        group   => "solr",
        source  => "/usr/local/src/abusesa-search/bin/solr-4.2-analysis-clarified.jar",
        require => [
            File["/srv/solr/cores/lib"],
            Util::Extract::Tar["/usr/local/src/abusesa-search"],
        ],
    }
    file { "/srv/solr/cores/lib/commons-net-3.1.jar":
        ensure  => present,
        mode    => "0660",
        owner   => "solr",
        group   => "solr",
        source  => "/usr/local/src/abusesa-search/sources/analysis-clarified/lib/commons-net-3.1.jar",
        require => [
            File["/srv/solr/cores/lib"],
            Util::Extract::Tar["/usr/local/src/abusesa-search"],
        ],
    }
    file { "/srv/solr/cores/lib/java-ipv6-0.8.jar":
        ensure  => present,
        mode    => "0660",
        owner   => "solr",
        group   => "solr",
        source  => "/usr/local/src/abusesa-search/sources/analysis-clarified/lib/java-ipv6-0.8.jar",
        require => [
            File["/srv/solr/cores/lib"],
            Util::Extract::Tar["/usr/local/src/abusesa-search"],
        ],
    }

    $htdocs = $::operatingsystem ? {
        "ubuntu" => "/usr/local/share/abusesa-search/htdocs",
        default  => "/usr/share/abusesa-search/htdocs",
    }

    define configwebhost($htdocs) {
        file { "/srv/www/https/${name}/search":
            ensure  => link,
            target  => $htdocs,
            require => File["/srv/www/https/${name}"],
        }
    }

    if $abusesa_search_webhosts {
        configwebhost { $abusesa_search_webhosts:
            htdocs => $htdocs,
        }
    }

}
