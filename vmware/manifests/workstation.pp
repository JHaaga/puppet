
# Install VMware Workstation
#
# Requires following files on puppet server:
#
#   /srv/puppet/files/common/packages/VMware-Workstation-Full.i386.bundle
#   /srv/puppet/files/common/packages/VMware-Workstation-Full.x86_64.bundle
#
# === Global variables
#
#   $vmware_serial:
#       Serial number for VMware Workstation.
#
class vmware::workstation {

    vmware::bundle { "VMware-Workstation-Full": }

    exec { "vmware-set-serial":
        command => "/usr/lib/vmware/bin/vmware-vmx --new-sn '${vmware_serial}'",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        require => Vmware::Bundle["VMware-Workstation-Full"],
    }

}
