# Install VMware bundle package and configure kernel modules
#
# === Parameters
#
#   $name:
#       Bundle package name to install.
#
# === Sample usage
#
# vmware::bundle { "VMware-Player": }
#
define vmware::bundle() {

    file { "/usr/local/src/${name}.bundle":
        ensure => present,
        source => "puppet:///files/packages/${name}.${architecture}.bundle",
        links  => follow,
        mode   => "0755",
        owner  => root,
        group  => root,
    }

    exec { "/usr/local/src/${name}.bundle --console --required":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        environment => "VMWARE_EULAS_AGREED=yes",
        refreshonly => true,
        subscribe   => File["/usr/local/src/${name}.bundle"],
        notify      => Exec["vmware-modconfig"],
    }

    include gcc
    exec { "vmware-modconfig":
        command => "vmware-modconfig --console --install-all",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        creates => "/lib/modules/${kernelrelease}/misc/vmci.ko",
        require => Class["gcc"],
    }

}
