
# Enable process accounting.
#
class psacct {

    case $::kernel {
        "linux": {
            include psacct::linux
        }
        "openbsd": {
            include psacct::openbsd
        }
        default: {
            fail("psacct module not supported in ${::kernel}")
        }
    }

}


# Enable process accounting on Linux hosts.
#
class psacct::linux {

    package { "psacct":
        name   => $::operatingsystem ? {
            "ubuntu" => "acct",
            default  => "psacct",
        },
        ensure => installed,
    }

    service { "psacct":
        name      => $::operatingsystem ? {
            "ubuntu" => "acct",
            default  => "psacct",
        },
        ensure    => $::operatingsystem ? {
            "ubuntu" => undef,
            default  => running,
        },
        enable    => true,
        hasstatus => true,
        require   => Package["psacct"],
    }

}


# Enable process accounting on OpenBSD hosts.
#
class psacct::openbsd {

    file { "/var/account/acct":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "wheel",
        notify => Exec["accton"],
    }

    service { "accounting":
        enable  => true,
        require => File["/var/account/acct"],
    }

    exec { "accton":
        command     => "accton /var/account/acct",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        user        => "root",
        refreshonly => true,
    }

}
