# Install and configure NTP daemon.
#
# === Global variables
#
#   $ntp_server:
#       Array of NTP servers using [] will disable external servers.
#       Defaults to pool.ntp.org.
#
#   $ntp_client_networks:
#       Array of networks that are allowed to query this server in format
#       [ "192.168.1.0/255.255.255.0", "192.168.2.0/255.255.255.0", ] or
#       [ "2001:838:0:1::/ffff:ffff:ffff:ffff::" ] for IPv6.
#
class ntpd {

    if !$ntp_server {
        $ntp_server = ["pool.ntp.org"]
    }

    case $::operatingsystem {
        "fedora": {
            case $::operatingsystemrelease {
                /^1[0-5]/: {
                    include ntpd::isc-ntpd
                }
                default: {
                    include ntpd::chrony
                }
            }
        }
        "openbsd": {
            include ntpd::openntpd
        }
        default: {
            include ntpd::isc-ntpd
        }
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            augeas { "disable-peerntp":
                context => "/files/etc/sysconfig/network",
                changes => "set PEERNTP no",
            }
        }
    }

}


# Install and configure Chrony NTP client.
#
class ntpd::chrony {

    package { "chrony":
        ensure => installed,
    }

    file { "/etc/chrony.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("ntpd/chrony.conf.erb"),
        require => Package["chrony"],
        notify  => Service["chronyd"],
    }

    service { "chronyd":
        ensure => running,
        enable => true,
    }

}


# Install and configure ISC NTP.
#
class ntpd::isc-ntpd {

    package { "ntp":
        ensure => installed,
    }

    file { "/etc/ntp.conf":
        ensure  => present,
        mode    => "0644",
        owner   => root,
        group   => root,
        content => template("ntpd/ntp.conf.erb"),
        require => Package["ntp"],
        notify  => Service["ntpd"],
    }

    service { "ntpd":
        ensure => running,
        enable => true,
        name   => $::operatingsystem ? {
            ubuntu  => "ntp",
            debian  => "ntp",
            default => "ntpd",
        },
    }

}


# Install and configure OpenNTPD.
#
class ntpd::openntpd {

    file { "/etc/ntpd.conf":
        ensure  => present,
        mode    => "0644",
        owner   => root,
        group   => wheel,
        content => template("ntpd/openntpd.conf.erb"),
        notify  => Service["ntpd"],
    }

    service { "ntpd":
        ensure  => running,
        enable  => true,
        binary  => "ntpd",
        start   => "ntpd -s",
        stop    => "pkill -u _ntp",
        status  => "pgrep -u _ntp",
        require => File["/etc/ntpd.conf"],
    }

}
