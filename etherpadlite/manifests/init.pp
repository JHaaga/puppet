# Install etherpad-lite.
#
class etherpadlite {

    include user::system
    realize(User["etherpad"], Group["etherpad"])

    file { "/var/lib/etherpad":
        ensure  => "directory",
        mode    => "0700",
        owner   => "etherpad",
        group   => "etherpad",
        require => User["etherpad"],
    }

}
