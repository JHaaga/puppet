
# Install MySQL client utilities.
#
class mysql::client {

    package { "mysql":
        ensure => installed,
        name   => $::operatingsystem ? {
            "ubuntu" => "mysql-client",
            default  => "mysql",
        },
    }

}


# Install MySQL server
#
# === Global variables
#
#   $mysql_datadir:
#       Directory where MySQL databases are stored.
#
#   $mysql_root_password:
#       Password for MySQL server root user.
#
class mysql::server {

    package { "mysql-server":
        ensure => installed,
    }

    if $mysql_datadir {
        file { $mysql_datadir:
            ensure  => directory,
            mode    => "0755",
            owner   => "mysql",
            group   => "mysql",
            seltype => "mysqld_db_t",
            require => Package["mysql-server"],
        }
        file { "/srv/mysql":
            ensure  => link,
            target  => $mysql_datadir,
            seltype => "mysqld_db_t",
            require => File[$mysql_datadir],
        }
    } else {
        file { "/srv/mysql":
            ensure  => directory,
            mode    => "0755",
            owner   => "mysql",
            group   => "mysql",
            seltype => "mysqld_db_t",
            require => Package["mysql-server"],
        }
    }

    if "${selinux}" == "true" {
        selinux::manage_fcontext { "/srv/mysql(/.*)?":
            type   => "mysqld_db_t",
            before => File["/srv/mysql"],
        }
        if $mysql_datadir {
            selinux::manage_fcontext { "${mysql_datadir}(/.*)?":
                type   => "mysqld_db_t",
                before => File[$mysql_datadir],
            }
        }
    }

    service { "mysqld":
        name    => $::operatingsystem ? {
            "ubuntu" => "mysql",
            "debian" => "mysql",
            default  => "mysqld",
        },
        ensure  => running,
        enable  => true,
        require => File["/srv/mysql"],
    }

    file { "/etc/my.cnf":
        ensure  => present,
        source  => [ "puppet:///files/mysql/my.cnf.${hostname}",
                     "puppet:///files/mysql/my.cnf",
                     "puppet:///modules/mysql/my.cnf", ],
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["mysql-server"],
        notify  => Service["mysqld"],
    }

    file { "/etc/logrotate.d/mysql":
        ensure  => present,
        source  => "puppet:///modules/mysql/mysql.logrotate",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["mysql-server"],
    }

    case $mysql_root_password {
        "": {
            file { "/var/lib/mysql":
                ensure  => directory,
                mode    => "0700",
                owner   => "mysql",
                group   => "mysql",
                require => Package["mysql-server"],
            }
        }
        default: {
            file { "/root/.my.cnf":
                ensure  => present,
                content => "[client]\nuser=\"root\"\npassword=\"${mysql_root_password}\"\n",
                mode    => "0600",
                owner   => "root",
                group   => $::operatingsystem ? {
                    "openbsd" => "wheel",
                    default   => "root",
                },
            }
        }
    }

}


# Install MySQL daily backup job
#
# === Global variables
#
#   $mysql_backup_datadir:
#       Directory where MySQL backups are stored. Defaults to /srv/mysql/backup
#
#   $mysql_backup_maxage:
#       How long to keep MySQL backups. Defaults to 168 hours (7 days).
#
class mysql::server::backup {

    include mysql::client

    if ! $mysql_backup_datadir {
        $mysql_backup_datadir = "/srv/mysql/backup"
    }
    if ! $mysql_backup_maxage {
        $mysql_backup_maxage = "168"
    }

    file { $mysql_backup_datadir:
        ensure  => directory,
        mode    => "0700",
        owner   => "root",
        group   => "root",
    }

    file { "/etc/cron.daily/mysql-backup":
        ensure  => present,
        content => template("mysql/mysql-backup.cron.erb"),
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => [ File[$mysql_backup_datadir],
                     Package["mysql"], ],
    }

}


# Install MySQL status reporting.
#
class mysql::server::report {

    package { "mysqlreport":
        ensure => installed,
    }

    file { "/var/log/mysql-report":
        ensure  => directory,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["mysql-server"],
    }

    file { "/etc/cron.daily/mysql-report":
        ensure  => present,
        source  => "puppet:///modules/mysql/mysql-report.cron",
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => File["/var/log/mysql-report"],
    }

}
