# Configure nagios target.
#
# === Parameters
#
#   $parent:
#       Parent hostname.
#
# === Global variables
#
#   $nagios_target_group:
#       Host and service group name. Defaults to $domain.
#
class nagios::target($parent=undef) {

    if $nagios_target_group {
        $group = $nagios_target_group
    } else {
        if $::domain {
            $group = $::domain
        } else {
            $group = "unknown"
        }
    }

    @@nagios::host { $::homename:
        group  => $group,
        osname => $::operatingsystem,
        osicon => $::osfamily ? {
            ""      => "NONE",
            default => inline_template("<%= @osfamily.downcase %>")
        },
        parent => $parent,
    }

    Nagios::Service {
        host  => $::homename,
        group => $group,
    }

}


# Configure dig service target.
#
# === Parameters
#
#     $query:
#         Address to query from DNS server. Defaults to localhost.
#
class nagios::target::dns ($query="localhost") inherits nagios::target {

    @@nagios::service { "${::homename}_dig":
        command     => "check_dig!${query}",
        description => "DNS",
    }

}


# Configure ssh service target.
#
class nagios::target::ssh inherits nagios::target {

    @@nagios::service { "${::homename}_ssh":
        command     => "check_ssh",
        description => "SSH",
    }

}


# Configure http service target.
#
class nagios::target::http inherits nagios::target {

    @@nagios::service { "${::homename}_http":
        command     => "check_http",
        description => "HTTP",
    }

}


# Configure https service target.
#
class nagios::target::https inherits nagios::target {

    @@nagios::service { "${::homename}_https":
        command     => "check_http!--ssl",
        description => "HTTPS",
    }

}


# Configure imaps service target.
#
class nagios::target::imaps inherits nagios::target {

    @@nagios::service { "${::homename}_imaps":
        command     => "check_imap!--ssl -p 993",
        description => "IMAPS",
    }

}


# Configure ldap service target.
#
class nagios::target::ldap inherits nagios::target {

    @@nagios::service { "${::homename}_ldap":
        command     => "check_ldap!${ldap_basedn}",
        description => "LDAP",
    }

}


# Configure ldaps service target.
#
class nagios::target::ldaps inherits nagios::target {

    @@nagios::service { "${::homename}_ldaps":
        command     => "check_ldap!${ldap_basedn} -p 636",
        description => "LDAPs",
    }

}


# Configure smtp service target.
#
class nagios::target::smtp inherits nagios::target {

    @@nagios::service { "${::homename}_smtp":
        command     => "check_smtp",
        description => "SMTP",
    }

}


# Configure tcp connect service target.
#
# === Parameters
#
#     $name:
#         Short name
#     $port:
#         Port where to connect
#     $description:
#         Description of service. Defaults to $name
#
# === Sample usage
#
# nagios::target::tcp { "git":
#     port        => "9418",
#     description => "GIT",
# }
#
define nagios::target::tcp($port, $description=undef) {

    include nagios::target

    if ! $description {
        $description = $name
    }

    @@nagios::service { "${::homename}_${name}":
        command     => "check_tcp!${port}",
        description => $description,
        group       => $nagios::target::group,
        host        => $::homename,
    }

}


# Configure nagios nrpe target.
#
class nagios::target::nrpe inherits nagios::target {

    if !$nagios_allow {
        $nagios_allow = "127.0.0.1"
    }

    include nagios::common

    case $operatingsystem {
        "centos","redhat","fedora": {
            $service = "nrpe"
            $nrpedir = "/etc/nrpe.d"
        }
        "ubuntu","debian": {
            $service = "nagios-nrpe-server"
            $nrpedir = "/etc/nagios/nrpe.d"
        }
        "openbsd": {
            $service = "nrpe"
            $nrpedir = "/etc/nrpe.d"
            exec { "add-nrpe-include-dir":
                command => "echo 'include_dir=${nrpedir}/' >> /etc/nrpe.cfg",
                path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                user    => "root",
                unless  => "egrep '^include_dir=${nrpedir}/' /etc/nrpe.cfg",
                require => Package["nrpe"],
                notify  => Service[$service],
                before  => File[$nrpedir],
            }
        }
    }

    package { "nrpe":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "nagios-nrpe-server",
            "ubuntu" => "nagios-nrpe-server",
            default  => "nrpe",
        }
    }

    file { "/etc/nrpe.d":
        ensure  => directory,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
    }

    service { "nrpe":
        name   => $service,
        ensure => running,
        enable => true,
    }

    file { "${nrpedir}/allowed_hosts.cfg":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => "allowed_hosts=${nagios_allow}\n",
        require => File["/etc/nrpe.d"],
        notify  => Service["nrpe"],
    }

    nagios::target::nrpe::service { "check_disk -w 10% -c 5% -p /":
        description => "Disk",
        package     => $::operatingsystem ? {
            "openbsd" => undef,
            "debian"  => "nagios-plugins-basic",
            "ubuntu"  => "nagios-plugins-basic",
            default   => "nagios-plugins-disk",
        }
    }
    nagios::target::nrpe::service { "check_load -r -w 3,2,1 -c 6,4,2":
        description => "Load",
        package     => $::operatingsystem ? {
            "openbsd" => undef,
            "debian"  => "nagios-plugins-basic",
            "ubuntu"  => "nagios-plugins-basic",
            default   => "nagios-plugins-load",
        }
    }
    nagios::target::nrpe::service { "check_swap -w 75% -c 50%":
        description => "Swap",
        package     => $::operatingsystem ? {
            "openbsd" => undef,
            "debian"  => "nagios-plugins-basic",
            "ubuntu"  => "nagios-plugins-basic",
            default   => "nagios-plugins-swap",
        }
    }

}


# Add new nagios nrpe service check
#
# === Parameters
#
#   $name:
#       Check command.
#   $description:
#       Service description. Defaults to command name without
#       check_ prefix.
#   $package:
#       Package providing check command.
#   $source:
#       Source file for check command.
#
# === Example usage
#
# nagios::target::nrpe::service { "check_disk -w 20% -c 10% -p /":
#     description => "Disk",
#     package     => $::operatingsystem ? {
#         "openbsd" => undef,
#         "debian"  => "nagios-plugins-basic",
#         "ubuntu"  => "nagios-plugins-basic",
#         default   => "nagios-plugins-disk",
#     }
# }
# 
define nagios::target::nrpe::service($source=undef,
                                     $description=undef,
                                     $package=undef) {

    include nagios::target::nrpe

    $binary = regsubst($name, '^([^ ]+) .*', '\1')
    $service = regsubst($binary, '^check_(.+)', '\1')

    if !$description {
        $description = $service
    }

    if $source {
        file { "${nagios::common::libdir}/${binary}":
            ensure  => present,
            source  => $source,
            mode    => "0755",
            owner   => "root",
            group   => $::operatingsystem ? {
                "openbsd" => "wheel",
                default   => "root",
            },
            require => Package["nrpe"],
            notify  => Service["nrpe"],
        }
    }

    if $package and !defined(Package[$package]) {
        package { $package:
            ensure  => present,
            require => Package["nrpe"],
            before  => Service["nrpe"],
        }
    }

    file { "${nagios::target::nrpe::nrpedir}/${binary}.cfg":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => "command[${binary}]=${nagios::common::libdir}/${name}\n",
        require => File["/etc/nrpe.d"],
        notify  => Service["nrpe"],
    }

    @@nagios::service { "${::homename}_${service}":
        command     => "check_nrpe!${binary}",
        description => $description,
        host        => $::homename,
    }

}

