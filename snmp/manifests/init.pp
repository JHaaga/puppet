
# Install net-snmp utilities
#
class snmp::utils {

    package { "net-snmp":
        name   => $::operatingsystem ? {
            "openbsd" => "net-snmp",
            default   => [ "net-snmp-utils", "net-snmp", ],
        },
        ensure => installed,
    }

}

# Install snmp trap daemon
#
# === Parameters
#
#   $acl:
#       Array containing list of allowed SNMP community names.
#       Defaults is to allow all trap messages.
#
# === Sample usage
#
# class { "snmp::trapd":
#     acl => [ "public", ],
# }
#
class snmp::trapd($acl=NONE) {

    require snmp::utils
    include user::system

    realize(User["snmptrapd"], Group["snmptrapd"])

    case $::operatingsystem {
        "centos","redhat": {
            file { "/etc/sysconfig/snmptrapd":
                ensure => present,
                source => "puppet:///modules/snmp/snmptrapd.sysconfig",
                mode   => "0644",
                owner  => "root",
                group  => "root",
                notify => Service["snmptrapd"],
            }
        }
        "openbsd": {
            file { "/etc/rc.d/snmptrapd":
                ensure => present,
                source => "puppet:///modules/snmp/snmptrapd.rc",
                mode   => "0755",
                owner  => "root",
                group  => "wheel",
                notify => Service["snmptrapd"],
            }
        }
        default: {
            fail("snmp::trapd not supported in ${::operatingsystem}")
        }
    }

    file { "/etc/snmptrapd.conf":
        ensure  => present,
        content => template("snmp/snmptrapd.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        notify  => Service["snmptrapd"],
    }

    service { "snmptrapd":
        ensure  => running,
        enable  => true,
        require => User["snmptrapd"],
    }

}
