# Install and configure nginx.
#
class nginx {

    case $::operatingsystem {
        "openbsd": {
            $user = "_nginx"
            $group = "_nginx"
        }
        default: {
            $user = "nginx"
            $group = "nginx"
        }
    }

    package { "nginx":
        ensure => installed,
    }

    service { "nginx":
        ensure  => running,
        enable  => true,
        require => Package["nginx"],
    }

    file { "/etc/nginx/nginx.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => template("nginx/nginx.conf.erb"),
        notify  => Service["nginx"],
        require => Package["nginx"],
    }

    file { "/etc/nginx/conf.d":
        ensure  => directory,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
        require => Package["nginx"],
    }

}


# Install and configure nginx with passenger.
#
class nginx::passenger inherits nginx {

    case $::operatingsystem {
        "openbsd": {
            Package["nginx"] {
                flavor => "passenger",
            }
            nginx::configfile { "passenger.conf":
                source => "puppet:///modules/nginx/passenger.conf",
            }
        }
        default: {
            fail("Not supported on ${::operatingsystem}")
        }
    }

}


# Add nginx configuration file.
#
define nginx::configfile($source="", $content="") {

    file { "/etc/nginx/conf.d/${name}":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        notify  => Service["nginx"],
        require => File["/etc/nginx/conf.d"],
    }

    if $source {
        File["/etc/nginx/conf.d/${name}"] {
            source => $source,
        }
    }

    if $content {
        File["/etc/nginx/conf.d/${name}"] {
            content => $content,
        }
    }

}
