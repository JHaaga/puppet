
# Install DenyHosts
#
class denyhosts {

    package { "denyhosts":
        ensure => installed,
    }

    service { "denyhosts":
        ensure  => running,
        enable  => true,
        require => Package["denyhosts"],
    }

}
