# Install mongodb client.
#
class mongodb::client {

    package { "mongodb":
        ensure => installed,
    }

}


# Install mongodb server.
#
class mongodb::server {

    package { "mongodb-server":
        ensure => installed,
    }

    if $mongodb_datadir {
        file { $mongodb_datadir:
            ensure  => directory,
            mode    => "0755",
            owner   => "mongodb",
            group   => "mongodb",
            require => Package["mongodb-server"],
            before  => File["/srv/mongodb"],
        }
        file { "/srv/mongodb":
            ensure  => link,
            target  => $mongodb_datadir,
        }
    } else {
        file { "/srv/mongodb":
            ensure  => directory,
            mode    => "0755",
            owner   => "mongodb",
            group   => "mongodb",
            require => Package["mongodb-server"],
        }
    }

    file { "/etc/mongodb.conf":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///modules/mongodb/mongodb.conf",
        notify => Service["mongod"],
    }

    selinux::manage_port { "28017":
        type  => "http_port_t",
        proto => "tcp",
    }

    service { "mongod":
        ensure  => running,
        enable  => true,
        require => File["/srv/mongodb"],
    }

}
