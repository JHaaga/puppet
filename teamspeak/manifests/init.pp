# Install TeamSpeak server.
#
# === Global variables
#
#   $teamspeak_license:
#       Teamspeak license file source.
#
class teamspeak {

    if !$teamspeak_package {
        case $::architecture {
            "amd64","x86_64": {
                if $teamspeak64_package_latest {
                    $teamspeak_package = $teamspeak64_package_latest
                } else {
                    fail("Must define \$teamspeak_package or \$teamspeak64_package_latest")
                }
            }
            default: {
                if $teamspeak32_package_latest {
                    $teamspeak_package = $teamspeak32_package_latest
                } else {
                    fail("Must define \$teamspeak_package or \$teamspeak32_package_latest")
                }
            }
        }
    }

    case $::architecture {
        "amd64","x86_64": {
            $teamspeak_bin = "/usr/local/teamspeak/ts3server_linux_amd64"
        }
        default: {
            $teamspeak_bin = "/usr/local/teamspeak/ts3server_linux_x86"
        }
    }

    file { "/usr/local/src/teamspeak3-server_linux.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///files/packages/${teamspeak_package}",
    }
    util::extract::tar { "/usr/local/teamspeak":
        strip   => 1,
        source  => "/usr/local/src/teamspeak3-server_linux.tar.gz",
        require => File["/usr/local/src/teamspeak3-server_linux.tar.gz"],
        notify  => Service["teamspeak"],
    }

    include user::system
    realize(User["teamspeak"], Group["teamspeak"])

    file { "/srv/teamspeak":
        ensure  => directory,
        mode    => "0700",
        owner   => "teamspeak",
        group   => "teamspeak",
        require => User["teamspeak"],
    }
    file { "/srv/teamspeak/ts3server.ini":
        ensure  => present,
        mode    => "0600",
        owner   => "teamspeak",
        group   => "teamspeak",
        source  => "puppet:///modules/teamspeak/ts3server.ini",
        require => File["/srv/teamspeak"],
        notify  => Service["teamspeak"],
    }

    if $teamspeak_license {
        file { "/srv/teamspeak/licensekey.dat":
            ensure  => present,
            mode    => "0600",
            owner   => "teamspeak",
            group   => "teamspeak",
            source  => $teamspeak_license,
            require => File["/srv/teamspeak"],
            notify  => Service["teamspeak"],
        }
    }

    file { "/etc/init.d/teamspeak":
        ensure  => present,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        content => template("teamspeak/teamspeak.init.erb"),
        notify  => Exec["add-service-teamspeak"],
    }
    exec { "add-service-teamspeak":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "chkconfig --add teamspeak",
        refreshonly => true,
        before      => Service["teamspeak"],
    }
    service { "teamspeak":
        ensure => running,
        enable => true,
    }

}
