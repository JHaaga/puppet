
class alpine {

    package { "alpine":
        ensure => installed,
    }

    if ! $mail_server {
        $mail_server = "mail.${domain}"
    }
    if ! $mail_domain {
        $mail_domain = $domain
    }

    file { "/usr/bin/pine":
        ensure  => link,
        target  => "/usr/bin/alpine",
        require => Package["alpine"],
    }

    file { "/etc/pine.conf.fixed":
        ensure  => present,
        name    => $::operatingsystem ? {
            "ubuntu" => "/etc/pinerc.fixed",
            default  => "/etc/pine.conf.fixed",
        },
        content => template("alpine/pine.conf.fixed.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["alpine"],
    }

}
