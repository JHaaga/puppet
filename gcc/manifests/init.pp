
# Install GCC
#
# On Linux hosts this will also install headers required for compiling
# kernel modules.
#
class gcc {

    package { "gcc":
        ensure => installed,
    }

    if $::kernel == Linux {
        package { "kernel-headers":
            name   => $::operatingsystem ? {
                "debian" => "linux-kernel-headers",
                "ubuntu" => "linux-kernel-headers",
                default  => [ "kernel-headers", "kernel-devel", ],
            },
            ensure => installed,
        }
    }

}
