#!/bin/sh

# Copyright (c) 2009 Timo Makinen
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# read samba config with testparm and determine path to print$ share
samba_driver_dir() {
    (echo "" | testparm) 2> /dev/null | awk '
        {
            if (/^\[print\$\]/) {
                sect=1
            } else if (/^\[/) {
                sect=0
            }
	    if (sect == 1 && /^[ \t]*path = /) {
	        print $3
            }
        }'
}

# function which replaces printer specific values from cups driver
parse_driver_inf() {
    cat "$1" | sed \
	-e "s/CUPS6\.\(PPD\|PS\)/${printer}.\1/g" \
	-e "s/CUPS Test Driver v\([0-9]*\)/CUPS Driver for ${printer}/"
}


CUPSDRIVERS="/usr/share/cups/drivers"
if [ ! -d "${CUPSDRIVERS}" ]; then
    echo "ERR: Cannot find CUPS driver directory ${CUPSDRIVERS}" 1>&2
    exit 1
fi
SAMBADRIVERS="`samba_driver_dir`"
if [ "${SAMBADRIVERS}" = "" ]; then
    echo 'ERR: Cannot find directory for share print$' 1>&2
    exit 1
elif [ ! -d "${SAMBADRIVERS}" ]; then
    echo 'ERR: Directory for share print$ does not exist' 1>&2
    exit 1
fi


# loop through printers
lpstat -p | sed -n 's/^printer \(.*\) is .*$/\1/p' | while read printer ; do
    for platform in "W32X86" "x64" "ia64" ; do
	# skip platform if no samba drivers are installed
	[ -d "${SAMBADRIVERS}/${platform}" ] || continue
	# set source inf and check that it's found
	if [ "${platform}" = "W32X86" ]; then
	    source="${CUPSDRIVERS}/cups6.inf"
	else
	    source="${CUPSDRIVERS}/${platform}/cups6.inf"
	fi
	[ -f "${source}" ] || continue
	
	# create new inf if needed
	target="${SAMBADRIVERS}/${platform}/${printer}.inf"
	if [ -f "${target}" ]; then
	    parse_driver_inf "${source}" | diff "${target}" - > /dev/null
	    if [ $? -eq 0 ]; then
		continue
	    fi
	fi
	echo "Updating driver INF file ${target}"
	parse_driver_inf "${source}" > "${target}"
    done
done
