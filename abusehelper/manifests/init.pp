# Install abusehelper.
#
# === Global variables
#
#   $abusehelper_datadir
#       Abusehelper home directory. Defaults to /var/lib/ah2.
#
#   $abusehelper_botnets
#       Array of botnet paths to start at boot.
#
#   $abusehelper_user
#       User botnets run as. Defaults to 'abusehel'.
#
class abusehelper {

    case $::operatingsystem {
        "ubuntu": {
            package { [ "python-pyme", "python-tz" ]:
                ensure => installed,
            }
        }
        "centos","redhat": {
            case $::operatingsystemrelease {
                /^5/: {
                    package { "python26":
                        ensure => installed,
                    }
                    Python::Setup::Install["/usr/local/src/abusehelper",
                                           "/usr/local/src/idiokit"] {
                        python  => "python2.6",
                        require => Package["python26"],
                    }
                }
            }
        }
    }

    if !$abusehelper_package {
        if $abusehelper_package_latest {
            $abusehelper_package = $abusehelper_package_latest
        } else {
            fail("Must define \$abusehelper_package or \$abusehelper_package_latest")
        }
    }

    if !$idiokit_package {
        if $idiokit_package_latest {
            $idiokit_package = $idiokit_package_latest
        } else {
            fail("Must define \$idiokit_package or \$idiokit_package_latest")
        }
    }

    file { "/usr/local/src/abusehelper.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source => "puppet:///files/packages/${abusehelper_package}",
    }
    util::extract::tar { "/usr/local/src/abusehelper":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/abusehelper.tar.gz",
        require => File["/usr/local/src/abusehelper.tar.gz"],
        before  => Python::Setup::Install["/usr/local/src/abusehelper"],
    }
    file { "/usr/local/src/idiokit.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source => "puppet:///files/packages/${idiokit_package}",
    }
    util::extract::tar { "/usr/local/src/idiokit":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/idiokit.tar.gz",
        require => File["/usr/local/src/idiokit.tar.gz"],
        before  => Python::Setup::Install["/usr/local/src/idiokit"],
    }
    python::setup::install { [ "/usr/local/src/abusehelper",
                               "/usr/local/src/idiokit", ]:
    }

    include user::system
    realize(User["abusehel"], Group["abusehel"])

    if $abusehelper_datadir {
        file { $abusehelper_datadir:
            ensure  => directory,
            mode    => "2770",
            owner   => "abusehel",
            group   => "abusehel",
            require => User["abusehel"],
        }

        file { "/var/lib/ah2":
            ensure  => link,
            target  => $abusehelper_datadir,
            require => File[$abusehelper_datadir],
        }
    } else {
        file { "/var/lib/ah2":
            ensure  => directory,
            mode    => "2770",
            owner   => "abusehel",
            group   => "abusehel",
            require => User["abusehel"],
        }
    }

    if $abusehelper_botnets {
        include abusehelper::init
    }

}


# Install abusehelper init script.
#
class abusehelper::init {

    if !$abusehelper_botnets {
        fail("Must define \$abusehelper_botnets")
    }

    if !$abusehelper_user {
        $abusehelper_user = "abusehel"
    }

    file { "/etc/sysconfig/botnet":
        ensure  => present,
        name    => $::operatingsystem ? {
            "debian" => "/etc/default/botnet",
            "ubuntu" => "/etc/default/botnet",
            default  => "/etc/sysconfig/botnet",
        },
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("abusehelper/botnet.sysconfig.erb"),
        before  => Service["botnet"],
    }

    file { "/etc/init.d/botnet":
        ensure => present,
        mode   => "0755",
        owner  => "root",
        group  => "root",
        source => "puppet:///modules/abusehelper/botnet.init",
        notify => Exec["add-service-botnet"],
    }
    exec { "add-service-botnet":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => $::operatingsystem ? {
            "debian" => "update-rc.d botnet defaults",
            "ubuntu" => "update-rc.d botnet defaults",
            default  => "chkconfig --add botnet",
        },
        refreshonly => true,
        before      => Service["botnet"],
    }

    service { "botnet":
        enable => true,
    }

}
