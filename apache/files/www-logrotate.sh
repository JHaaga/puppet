#!/bin/bash

# Copyright (c) 2005 Marko Laakso in adm@ee.oulu.fi role

# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# $Id: dologs.sh,v 1.8 2008/09/12 11:07:57 fenris Exp $

umask 022
SUFFIX=`date +%Y%m%d-%H%M`

if [ -x /usr/sbin/lsof ]; then
  LSOF=/usr/sbin/lsof
else
  LSOF=lsof
fi

if [ -r /var/run/httpd.pid ]; then
    HTTPDPID=`cat /var/run/httpd.pid`
elif [ -r /var/run/httpd/httpd.pid ]; then
    HTTPDPID=`cat /var/run/httpd/httpd.pid`
elif [ -r /var/run/apache2.pid ]; then
    HTTPDPID=`cat /var/run/apache2.pid`
fi

if [ -r /var/run/httpsd.pid ]; then
    HTTPSDPID=`cat /var/run/httpsd.pid`
elif [ -r /var/run/httpd/httpsd.pid ]; then
    HTTPSDPID=`cat /var/run/httpd/httpsd.pid`
fi

signalapache() {

  pid=$1

  kill -CONT $pid 1> /dev/null 2>&1

  if [ $? -ne 0 ]; then
    echo "no such apache [$pid] before signal - manual start req" 1>&2
    exit 1
  fi

  kill -HUP $pid 1> /dev/null 2>&1
  sleep 1
  kill -CONT $pid 1> /dev/null 2>&1

  if [ $? -ne 0 ]; then
    echo "no such apache [$pid] after signal - manual start req" 1>&2
    exit 1
  fi
}

for pid in ${HTTPDPID} ${HTTPSDPID}; do
  for log in `${LSOF} -p ${pid} | grep /www/log | \
              awk '{ print $9 }' | sort | uniq`; do
    [ -f ${log} ] || continue
    mv $log ${log}.${SUFFIX}
    mylogs="${mylogs} ${log}.${SUFFIX}"
  done
done

[ -z "${HTTPDPID}" ] || signalapache ${HTTPDPID}
[ -z "${HTTPSDPID}" ] || signalapache ${HTTPSDPID}

for log in ${mylogs}; do
  gzip $log
done
