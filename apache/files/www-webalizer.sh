#!/bin/sh

if [ `whoami` != webalizer ]; then
    echo "ERR: Script needs to be run as webalizer user" 1>&2
    exit 1
fi

umask 022

for cfg in /etc/webalizer/http/*.conf /etc/webalizer/https/*.conf; do
    test -f ${cfg} && /usr/bin/webalizer -c ${cfg}
done
