class apache::debian::common {

    if ! $httpd_user {
        $httpd_user = "www-data"
    }
    if ! $httpd_group {
        $httpd_group = "www-data"
    }

    file { [ "/srv/www/http",
             "/srv/www/http/${homename}",
             "/srv/www/log/http",
             "/srv/www/log/http/${homename}",
             "/etc/apache2/conf.d",
             "/etc/apache2/sites-enabled", ]:
        ensure  => directory,
        mode    => "0644",
        owner   => root,
        group   => root,
        require => Package["httpd"],
        before  => File["/etc/apache2/apache2.conf"],
    }

    File["/etc/apache2/conf.d", "/etc/apache2/sites-enabled"] {
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
    }

    file { "/etc/apache2/envvars":
        ensure  => present,
        content => template("apache/apache2.envvars.erb"),
        mode    => "0644",
        owner   => root,
        group   => root,
        before  => File["/etc/apache2/apache2.conf"],
        require => Package["httpd"],
        notify  => Service["apache2"],
    }

    file { "/etc/apache2/httpd.conf":
        ensure  => present,
        content => template("apache/apache2.httpd.conf.erb"),
        mode    => "0644",
        owner   => root,
        group   => root,
        before  => File["/etc/apache2/apache2.conf"],
        require => Package["httpd"],
        notify  => Service["apache2"],
    }

    file { "/etc/apache2/ports.conf":
        ensure  => present,
        content => "# HTTP server disabled\n",
        mode    => "0644",
        owner   => root,
        group   => root,
        before  => File["/etc/apache2/apache2.conf"],
        require => Package["httpd"],
        notify  => Service["apache2"],
    }

    file { "/etc/apache2/apache2.conf":
        ensure  => present,
        content => template("apache/apache2.conf.erb"),
        mode    => "0644",
        owner   => root,
        group   => root,
        notify  => Service["apache2"],
    }

    service { "apache2":
        ensure  => running,
        enable  => true,
        require => File["/etc/apache2/apache2.conf"],
    }

}


class apache::debian::server inherits apache::debian::common {

    File["/etc/apache2/ports.conf"] {
        content => "NameVirtualHost *:80\nListen 80\n"
    }

}


define apache::debian::site($aliases, $root, $redirect) {

    if $name == "default" {
        $site_fqdn = $homename
        $site_conf = "/etc/apache2/sites-enabled/00-${site_fqdn}.conf"
        $site_confdir = "/etc/apache2/sites-enabled/00-${site_fqdn}.d"
    } else {
        $site_fqdn = $name
        $site_conf = "/etc/apache2/sites-enabled/${site_fqdn}.conf"
        $site_confdir = "/etc/apache2/sites-enabled/${site_fqdn}.d"

        if !$redirect {
            if !$proxy {
                if $root {
                    file { "/srv/www/http/${site_fqdn}":
                        ensure => link,
                        target => $root,
                        before => File[$site_conf],
                    }
                } else {
                    file { "/srv/www/http/${site_fqdn}":
                        ensure => directory,
                        mode   => "0755",
                        owner  => root,
                        group  => root,
                        before => File[$site_conf],
                    }
                }
            }

            file { "/srv/www/log/http/${site_fqdn}":
                ensure => directory,
                mode   => "0755",
                owner  => root,
                group  => root,
                before => File[$site_conf],
            }
        }
    }

    file { $site_conf:
        ensure => present,
        mode   => "0644",
        owner  => root,
        group  => root,
        notify => Service["apache2"],
    }

    if $redirect {
        File[$site_conf] {
            content => "<VirtualHost *:80>\n  ServerName ${site_fqdn}\n  Redirect permanent / ${redirect}\n</VirtualHost>\n",
        }
    } elsif $proxy {
        File[$site_conf] {
            content => template("apache/proxy.http.conf.erb"),
        }
    } else {
        File[$site_conf] {
            content => template("apache/site.http.conf.erb"),
        }
        file { $site_confdir:
            ensure  => directory,
            mode    => "0644",
            owner   => root,
            group   => root,
            purge   => true,
            force   => true,
            recurse => true,
            source  => [ "puppet:///files/apache/sites/${site_fqdn}",
                         "puppet:///modules/custom/empty", ],
            before  => File[$site_conf],
            notify  => Service["apache2"],
        }
    }

}



class apache::debian::sslserver inherits apache::debian::common {

    file { [ "/srv/www/https",
             "/srv/www/https/${homename}",
             "/srv/www/log/https",
             "/srv/www/log/https/${homename}", ]:
        ensure  => directory,
        mode    => "0644",
        owner   => root,
        group   => root,
        require => Package["httpd"],
        before  => File["/etc/apache2/apache2.conf"],
    }

    apache::debian::a2enmod { "ssl": }

}


define apache::debian::sslsite($first, $ipaddr, $root,
                               $ssl_cert, $ssl_key, $ssl_chain) {

    if $name == "default" {
        $site_fqdn = $homename
    } else {
        $site_fqdn = $name

        if $root {
            file { "/srv/www/https/${site_fqdn}":
                ensure => link,
                target => $root,
                before => Service["apache2"],
            }
        } else {
            file { "/srv/www/https/${site_fqdn}":
                ensure => directory,
                mode   => "0755",
                owner  => root,
                group  => root,
                before => Service["apache2"],
            }
        }

        file { "/srv/www/log/https/${site_fqdn}":
            ensure => directory,
            mode   => "0755",
            owner  => root,
            group  => root,
            before => Service["apache2"],
        }
    }

    if $ssl_cert {
        $real_ssl_cert = $ssl_cert
    } else {
        $real_ssl_cert = "${puppet_ssldir}/certs/${homename}.pem"
    }

    file { "/etc/ssl/certs/${site_fqdn}.crt":
        ensure => present,
        source => $real_ssl_cert,
        mode   => "0644",
        owner  => root,
        group  => root,
        notify => Service["apache2"],
    }

    if $ssl_key {
        $real_ssl_key = $ssl_key
    } else {
        $real_ssl_key = "${puppet_ssldir}/private_keys/${homename}.pem"
    }

    file { "/etc/ssl/private/${site_fqdn}.key":
        ensure => present,
        source => $real_ssl_key,
        mode   => "0600",
        owner  => root,
        group  => root,
        notify => Service["apache2"],
    }

    if $ssl_chain {
        file { "/etc/ssl/certs/${site_fqdn}.chain.crt":
            ensure => present,
            source => $ssl_chain,
            mode   => "0644",
            owner  => root,
            group  => root,
            notify => Service["apache2"],
        }
    }

    if $first == true {
        $site_conf = "/etc/apache2/sites-enabled/00-${site_fqdn}-ssl.conf"
        $site_confdir = "/etc/apache2/sites-enabled/00-${site_fqdn}-ssl.d"
    } else {
        $site_conf = "/etc/apache2/sites-enabled/${site_fqdn}-ssl.conf"
        $site_confdir = "/etc/apache2/sites-enabled/${site_fqdn}-ssl.d"
    }

    file { $site_conf:
        ensure  => present,
        content => template("apache/site.https.conf.erb"),
        mode    => "0644",
        owner   => root,
        group   => root,
        notify  => Service["apache2"],
        require => [ File["/etc/ssl/certs/${site_fqdn}.crt"],
                     File["/etc/ssl/private/${site_fqdn}.key"], ],
    }

    file { $site_confdir:
        ensure  => directory,
        mode    => "0644",
        owner   => root,
        group   => root,
        purge   => true,
        force   => true,
        recurse => true,
        source  => [ "puppet:///files/apache/sslsites/${site_fqdn}",
                     "puppet:///modules/custom/empty", ],
        before  => File[$site_conf],
        notify  => Service["apache2"],
    }

}


define apache::debian::configfile($source, $content, $http, $https) {

    file { "/etc/apache2/conf.d/${name}":
        ensure  => present,
        mode    => "0644",
        owner   => root,
        group   => root,
        notify  => Service["apache2"],
        require => Package["httpd"],
    }
    if $source {
        File["/etc/apache2/conf.d/${name}"] {
            source => $source,
        }
    }
    if $content {
        File["/etc/apache2/conf.d/${name}"] {
            content => $content,
        }
    }

}


# Enable module on Debian/Ubuntu Apache.
#
# === Parameters
#
#   $name:
#       Module name.
#   $source:
#       Source for optional module configuration.
#   $content:
#       Content for optional module configuration.
#       See also $source.
#
define apache::debian::a2enmod($source="", $content="") {

    exec { "a2enmod-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "a2enmod ${name}",
        unless  => "test -h /etc/apache2/mods-enabled/${name}.load",
        notify  => Service["apache2"],
        require => Package["httpd"],
    }

    if $source or $content {
        file { "/etc/apache2/mods-available/${name}.conf":
            mode    => "0644",
            owner   => root,
            group   => root,
            notify  => Service["apache2"],
            before  => Exec["a2enmod-${name}"],
            require => Package["httpd"],
        }
        if $source {
            File["/etc/apache2/mods-available/${name}.conf"] {
                source => $source,
            }
        }
        if $content {
            File["/etc/apache2/mods-available/${name}.conf"] {
                content => $content,
            }
        }
    }

}
