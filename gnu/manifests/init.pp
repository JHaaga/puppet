# Install GNU make.
#
class gnu::make {

    package { "make":
        ensure => installed,
        name   => $::operatingsystem ? {
            "openbsd" => "gmake",
            default   => "make",
        },
    }

}


# Install GNU tar.
#
class gnu::tar {

    package { "tar":
        ensure => installed,
        name   => $::operatingsystem ? {
            "openbsd" => "gtar",
            default   => "tar",
        },
    }

}
