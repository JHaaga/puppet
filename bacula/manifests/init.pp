
# Install puppet certificates to be used by Bacula
#
class bacula::certificates {

    file { "/etc/pki/tls/private/bacula.key":
        ensure => present,
        source => "${::puppet_ssldir}/private_keys/${::homename}.pem",
        mode   => "0640",
        owner  => "root",
        group  => "bacula",
    }

    file { "/etc/pki/tls/certs/bacula.crt":
        ensure => present,
        source => "${::puppet_ssldir}/certs/${::homename}.pem",
        mode   => "0644",
        owner  => "root",
        group  => "root",
    }

}


# Install bacula client
#
# === Parameters
#
#     $password:
#         Password used by director for connecting. Defaults
#         to md5 hash from puppet fqdn_rand function output.
#
class bacula::client($password=undef) {

    if !$password {
        $password_real = md5(fqdn_rand(99999999999999999999999999))
    } else {
        $password_real = $password
    }

    include bacula::certificates

    package { "bacula-client":
        ensure => installed,
        before => Class["bacula::certificates"],
    }

    file { "/etc/bacula/bacula-fd.conf":
        ensure  => present,
        content => template("bacula/bacula-fd.conf.erb"),
        mode    => 0640,
        owner   => "root",
        group   => "bacula",
        require => Package["bacula-client"],
        notify  => Service["bacula-fd"],
    }

    @@file { "/etc/bacula/bacula-dir.d/client-${homename}.conf":
        ensure  => present,
        content => template("bacula/client.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "bacula",
        tag     => "bacula",
    }

    service { "bacula-fd":
        ensure  => running,
        enable  => true,
        require => Class["bacula::certificates"],
    }

}


# Install Bacula console
#
# Note that console will be able to connect all defined Bacula
# directors.
#
class bacula::console {

    include bacula::certificates

    package { "bacula-console":
        ensure => installed,
        before => Class["bacula::certificates"],
    }

    File <<| tag == "bacula-console" |>>

}


# Install Bacula director
#
# === Parameters
#
#     $datadir:
#         Data directory for storing database and bootstraps.
#         Defaults to /srv/bacula.
#     $dbadapter:
#         Database type for catalog. Only sqlite and mysql are
#         supported. Defaults to sqlite.
#     $dbserver:
#         Database server address. Defaults to "localhost". Not needed
#         for sqlite.
#     $dbname:
#         Database name. Defaults to "bacula". Not needed for sqlite.
#     $dbuser:
#         Database user name. Defaults to "bacula". Not needed for sqlite.
#     $dbpassword:
#         Database password. Not needed for sqlite.
#     $password:
#         Password required for connecting to director. Defaults
#         to md5 hash from puppet fqdn_rand function output.
#
class bacula::director($password=undef,
                       $datadir="/srv/bacula",
                       $dbadapter="sqlite",
                       $dbserver="localhost",
                       $dbname="bacula",
                       $dbuser="bacula",
                       $dbpassword=undef) {

    include bacula::certificates
    include bacula::console

    if !$password {
        $password_real = md5(fqdn_rand(99999999999999999999999999))
    } else {
        $password_real = $password
    }

    case $dbadapter {
        "sqlite": {
            exec { "create-bacula-catalog":
                command => "/usr/libexec/bacula/make_sqlite3_tables && mv /var/spool/bacula/bacula.db /srv/bacula",
                user    => "bacula",
                path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                creates => "/srv/bacula/bacula.db",
                require => File["/srv/bacula"],
                before  => Service["bacula-director"],
            }
        }
        "mysql": {
            require mysql::client
            if !$dbpassword {
                fail("\$dbpassword is required for bacula::director when using '${dbadapter} database")
            }
        }
        "postgresql": {
            require postgresql::client
            if !$dbpassword {
                fail("\$dbpassword is required for bacula::director when using '${dbadapter} database")
            }
        }
        default: {
            fail("Unknown \$dbadapter for bacula::director")
        }
    }

    package { "bacula-director":
        name   => $dbadapter ? {
            "mysql"      => "bacula-director-mysql",
            "postgresql" => "bacula-director-postgresql",
            "sqlite"     => "bacula-director-sqlite",
        },
        ensure => installed,
        before => Class["bacula::certificates"],
    }

    file { "/etc/bacula/bacula-dir.conf":
        ensure  => present,
        content => template("bacula/bacula-dir.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "bacula",
        require => Package["bacula-director"],
        notify  => Service["bacula-director"],
    }
    file { "/etc/bacula/bacula-dir.d":
        ensure  => directory,
        mode    => "0750",
        owner   => "root",
        group   => "bacula",
        purge   => true,
        recurse => true,
        require => Package["bacula-director"],
        notify  => Service["bacula-director"],
    }

    @@file { "/etc/bacula/bconsole.conf":
        ensure  => present,
        content => template("bacula/bconsole.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "bacula",
        tag     => "bacula-console",
        require => Package["bacula-console"],
    }

    file { "/etc/sysconfig/bacula-dir":
        ensure  => present,
        content => "DIR_USER=bacula\nDIR_GROUP=bacula\n",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["bacula-director"],
        notify  => Service["bacula-director"],
    }

    file { $datadir:
        ensure  => directory,
        mode    => "0770",
        owner   => "bacula",
        group   => "bacula",
        seltype => "var_spool_t",
        require => Package["bacula-director"],
    }
    selinux::manage_fcontext { "${datadir}(/.*)?":
        type   => "var_spool_t",
        before => File[$datadir],
    }  
    if $datadir != "/srv/bacula" {
        file { "/srv/bacula":
            ensure  => link,
            target  => $datadir,
            owner   => "bacula",
            group   => "bacula",
            seltype => "var_spool_t",
            require => File[$datadir],
        }
        selinux::manage_fcontext { "/srv/bacula(/.*)?":
            type   => "var_spool_t",
            before => File[$datadir],
        }
    }

    File <<| tag == "bacula" |>> {
        require => File["/etc/bacula/bacula-dir.d"],
        notify  => Service["bacula-director"],
    }

    # catalog backup job (also runs db compacting)
    bacula::fileset { "Catalog":
        source => "puppet:///modules/bacula/fileset.Catalog.conf",
    }
    bacula::job { "BackupCatalog":
        options => [
          "Level = Full",
          "FileSet = Catalog",
          'RunBeforeJob = "sh -c /usr/local/sbin/bacula_catalog_dump"',
          'RunAfterJob  = "rm /srv/bacula/bacula.sql"',
          'Write Bootstrap = "/srv/bacula/%n.bsr"',
          "Priority = 11",
        ],
    }
    file { "/usr/local/sbin/bacula_catalog_dump":
        ensure => present,
        source => "puppet:///modules/bacula/bacula_catalog_dump",
        mode   => "0755",
        owner  => "root",
        group  => "root",
        before => Service["bacula-director"],
    }

    service { "bacula-director":
        name    => "bacula-dir",
        ensure  => running,
        enable  => true,
        require => [ File["/srv/bacula"], Class["bacula::certificates"], ],
    }

}


# Create new Bacula fileset
#
# === Parameters
#
#     $name:
#         Fileset name.
#     $source:
#         Path to fileset source file. Defaults to
#         "puppet:///files/bacula/fileset.${name}.conf".
#
# === Sample usage
#
# bacula::fileset { "Default":
#     source => "puppet:///modules/bacula/fileset.Default.conf",
# }
#
define bacula::fileset($source="puppet:///files/bacula/fileset.${name}.conf") {

    file { "/etc/bacula/bacula-dir.d/fileset-${name}.conf":
        ensure  => present,
        source  => $source,
        mode    => "0640",
        owner   => "root",
        group   => "bacula",
        require => File["/etc/bacula/bacula-dir.d"],
        notify  => Service["bacula-dir"],
    }

}


# Create new Bacula job
#
# === Parameters
#
#     $name:
#         Job name
#     $jobdefs:
#         Resource where default values for job are taken.
#         Defaults to "DefaultJob".
#
# === Sample usage
#
# bacula::job { $homename: }
#
define bacula::job($jobdefs="DefaultJob", $options=[]) {

    @@file { "/etc/bacula/bacula-dir.d/job-${name}.conf":
        ensure  => present,
        content => template("bacula/job.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "bacula",
        tag     => "bacula",
    }

}


# Install Bacula storage daemon
#
# === Parameters
#
#     $password:
#         Password required for conneting to this storage daemon.
#         Defaults to md5 hash from puppet fqdn_rand function output.
#
class bacula::storage($password = undef) {

    if !$password {
        $password_real = md5(fqdn_rand(99999999999999999999999999))
    } else {
        $password_real = $password
    }

    include bacula::certificates

    package { "bacula-storage":
        name   => "bacula-storage-sqlite",
        ensure => installed,
        before => Class["bacula::certificates"],
    }

    file { "/etc/sysconfig/bacula-sd":
        ensure  => present,
        content => "SD_USER=bacula\nSD_GROUP=bacula\n",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["bacula-storage"],
        notify  => Service["bacula-sd"],
    }
    exec { "usermod-bacula":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "usermod -a -G tape bacula",
        unless  => "id -n -G bacula | grep '\\btape\\b'",
        require => Package["bacula-storage"],
        before  => Service["bacula-sd"],
    }

    file { "/etc/bacula/bacula-sd.conf":
        ensure  => present,
        content => template("bacula/bacula-sd.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "bacula",
        require => Package["bacula-storage"],
        notify  => Service["bacula-sd"],
    }

    file { "/etc/bacula/bacula-sd.d":
        ensure  => directory,
        mode    => "0750",
        owner   => "root",
        group   => "bacula",
        purge   => true,
        recurse => true,
        require => Package["bacula-storage"],
    }

    service { "bacula-sd":
        ensure  => running,
        enable  => true,
        require => Class["bacula::certificates"],
    }

}


# Create new backup device
#
# === Parameters
#
#     $name:
#         Name for backup device.
#     $device:
#         Filename for storage device (eg. /dev/nst0).
#     $media:
#         Media type supported by this device.
#
# === Sample usage
#
# bacula::device { "Tape":
#     device => "/dev/nst0",
#     media  => "LTO3",
# }
#         
define bacula::device($device, $media) {

    include bacula::storage
    require bacula::device::common

    file { "/etc/bacula/bacula-sd.d/${name}.conf":
        ensure  => present,
        content => template("bacula/device.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "bacula",
        require => File["/etc/bacula/bacula-sd.d"],
        notify  => Service["bacula-sd"],
    }

    @@file { "/etc/bacula/bacula-dir.d/device-${name}.conf":
        ensure  => present,
        content => template("bacula/storage.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "bacula",
        tag     => "bacula",
    }

}


class bacula::device::common {

    require smart
    package { "mtx":
        ensure => installed,
    }

    file { "/usr/local/sbin/tapealert":
        ensure  => present,
        source  => "puppet:///modules/bacula/tapealert",
        mode    => "0755",
        owner   => "root",
        group   => "root",
    }

}


# Install Bacula web interface
#
# === Parameters
#
#     $htaccess:
#         Source path for htaccess file used for authentication and
#         authorization.
#     $webhosts:
#         Array containing virtual host names where bacula web will
#         will be installed.
#     $dbadapter:
#         Database type for catalog. Only sqlite and mysql are
#         supported. Defaults to sqlite.
#     $dbserver:
#         Database server address. Defaults to "localhost". Not needed
#         for sqlite.
#     $dbname:
#         Database name. Defaults to "bacula". Not needed for sqlite.
#     $dbuser:
#         Database user name. Defaults to "bacula". Not needed for sqlite.
#     $dbpassword:
#         Database password. Not needed for sqlite.
#
class bacula::web($htaccess,
                  $webhosts=undef,
                  $dbadapter="sqlite",
                  $dbserver="localhost",
                  $dbname="bacula",
                  $dbuser="bacula",
                  $dbpassword=undef) {

    if $dbadapter != "sqlite" and !$dbpassword {
        fail("\$dbpassword is required for bacula::web when using '${dbadapter}' database")
    }

    case $dbadapter {
        "mysql": {
            class { "php::mysql":
                before => File["/usr/local/src/bacula-web.tar.gz"],
            }
        }
        "postgresql": {
            class { "php::pgsql":
                before => File["/usr/local/src/bacula-web.tar.gz"],
            }
        }
        "sqlite": { }
        default: {
            fail("Invalid \$dbadapter '${dbadapter}' in bacula::web")
        }
    }

    include php::gd
    include php::pdo

    selinux::manage_fcontext { "/usr/local/share/bacula-web(/.*)?":
        type   => "httpd_sys_content_t",
        before => Util::Extract::Tar["/usr/local/share/bacula-web"],
    }
    selinux::manage_fcontext { "/usr/local/share/bacula-web/application/view/cache(/.*)?":
        type    => "httpd_sys_rw_content_t",
        require => Selinux::Manage_fcontext["/usr/local/share/bacula-web(/.*)?"],
        before  => Util::Extract::Tar["/usr/local/share/bacula-web"],
    }

    file { "/usr/local/src/bacula-web.tar.gz":
        ensure => present,
        source => "puppet:///files/packages/${bacula_package_latest}",
        mode   => "0644",
        owner  => "root",
        group  => "root",
        require => [ Class["php::gd"], Class["php::pdo"], ],
    }
    util::extract::tar { "/usr/local/share/bacula-web":
        ensure  => latest,
        source  => "/usr/local/src/bacula-web.tar.gz",
        require => File["/usr/local/src/bacula-web.tar.gz"],
    }

    file { "/usr/local/share/bacula-web/.htaccess":
        ensure  => present,
        source  => $htaccess,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        seltype => "httpd_sys_content_t",
        require => Util::Extract::Tar["/usr/local/share/bacula-web"],
    }

    file { "/usr/local/share/bacula-web/application/config/config.php":
        ensure  => present,
        content => template("bacula/bacula-web.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        seltype => "httpd_sys_content_t",
        require => File["/usr/local/share/bacula-web/.htaccess"],
    }

    file { "/usr/local/share/bacula-web/application/view/cache":
        ensure  => directory,
        mode    => "0770",
        owner   => $apache::sslserver::group,
        group   => $apache::sslserver::group,
        seltype => "httpd_sys_rw_content_t",
        require => Util::Extract::Tar["/usr/local/share/bacula-web"],
    }

    define configwebhost() {
        file { "/srv/www/https/${name}/bacula":
            ensure  => link,
            target  => "/usr/local/share/bacula-web",
            owner   => "root",
            group   => "root",
            require => File["/srv/www/https/${name}"],
        }
    }
    if $webhosts {
        configwebhost { $webhosts: }
    }

}
