
class custom {

    file { "/srv":
        ensure => directory,
        mode   => "0755",
        owner  => root,
        group  => $::operatingsystem ? {
            OpenBSD => wheel,
            default => root,
        },
    }

    if $kernel == OpenBSD {
        Service {
            provider => openbsd,
        }
    }

}


# Extended version of default File type.
#
# === Parameters:
#
#   $ensure:
#       See $ensure from File type.
#   $group:
#       See $group from File type.
#   $mode:
#       See $mode from File type.
#   $owner:
#       See $owner from File type.
#   $seltype:
#       See $seltype from File type.
#   $source:
#       See $source from File type. This define will also accept http,
#       https and ftp urls.
#
# === Sample usage:
#
# custom::file { "/usr/src/puppet.tar.gz":
#     ensure => present,
#     source => "http://puppetlabs.com/downloads/puppet/puppet-2.6.2.tar.gz",
# }
#
define custom::file($ensure, $group="NONE", $mode="NONE", $owner="NONE", $seltype="NONE", $source) {

    $test = regsubst($source, '^([^:]+)://.+$', '\1')
    if "${test}" == "${source}" {
        $method = "file"
        $path = $source
    } else {
        $method = $test
    }

    case $method {
        "ftp","http","https": {
            $fetch_cmd = "wget -q -O '${name}' '${source}'"
            $diff_cmd = "wget -N --spider '${source}' 2>&1 | fgrep 'Server file no newer than local file'"
        }
    }

    case $method {
        "file":   {}
        "puppet": {}
        default:  {
            exec { "fetch-file-${source}":
                environment => $http_proxy ? {
                    ""      => undef,
                    default => "http_proxy=${http_proxy}",
                },
                path        => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
                cwd         => regsubst($name, '(.*)/[^/]+$', '\1'),
                command     => $fetch_cmd,
                unless      => $diff_cmd,
                before      => File[$name],
            }
        }
    }

    file { $name:
        ensure  => $ensure,
        source  => $method ? {
            "file"   => $path,
            "puppet" => $source,
            default  => undef,
        },
        mode    => $mode ? {
            "NONE"  => undef,
            default => $mode,
        },
        owner   => $owner ? {
            "NONE"  => undef,
            default => $owner,
        },
        group   => $group ? {
            "NONE"  => undef,
            default => $group,
        },
        seltype => $seltype ? {
            "NONE"  => undef,
            default => $seltype,
        },
    }

}


# Set root password
#
# === Global variables
#
#   $root_password:
#       Root password hash to set.
#
class custom::rootpassword {

    if ! $root_password {
        fail("Root password hash not defined.")
    }

    case $::operatingsystem {
        openbsd: {
            exec { "usermod -p \${SECRET} root":
                environment => "SECRET=${root_password}",
                path        => "/bin:/usr/bin:/sbin:/usr/sbin",
                unless      => 'test "`sed -n \'s/^root:\([a-zA-Z0-9\.\$]*\):.*/\1/p\' /etc/master.passwd`" = "${SECRET}"',
            }
        }
        default: {
            user { "root":
                ensure   => present,
                password => $root_password,
            }
        }
    }

}
