
Facter.add('console_type') do
    setcode do
	begin
	    Facter.kernel
	rescue
	    Facter.loadfacts()
	end
	console = 'vga'
	kernel = Facter.value('kernel')
	if kernel == 'Linux'
	    File.open('/proc/cmdline', 'r') do |f|
		while line = f.gets
		    if line =~ /console=ttyS[0-9]/
			console = 'serial'
		    end
		end
	    end
	elsif kernel == 'OpenBSD' and File.exists?('/etc/boot.conf')
	    File.open('/etc/boot.conf', 'r') do |f|
	        while line = f.gets
		    if line =~ /^set tty com[0-9]$/
		        console = 'serial'
		    end
		end
	    end
	end
	console
    end
end
