require 'puppet'

Facter.add('homename') do
    setcode do
        if Facter.value('puppetversion').to_i < 3
            Puppet.parse_config
        end
        Puppet.settings.value('certname')
    end
end
