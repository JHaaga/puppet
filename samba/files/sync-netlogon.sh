#!/bin/sh

# determine domain name
WORKGROUP=`echo '' | testparm -v --section-name global 2> /dev/null | \
    sed -n 's/^\tworkgroup = \(.*\)/\1/p'`
if [ "${WORKGROUP}" = "" ]; then
    echo "Error in netlogon sync, failed to determine domain name"
    exit 1
fi

WINSSERVER=`echo '' | testparm -v --section-name global 2> /dev/null | \
    sed -n 's/^\twins server = \(.*\)/\1/p'`
if [ "${WINSSERVER}" = "" ]; then
    NMBLOOKUP="nmblookup"
else
    NMBLOOKUP="nmblookup -r ${WINSSERVER}"
fi
PDC=`${NMBLOOKUP} "${WORKGROUP}#1B" | \
    sed -n "s/^\([0-9\.]*\) ${WORKGROUP}<1b>/\1/p" | head -1`
if [ "${PDC}" = "" ]; then
    echo "Error in netlogon sync, failed to determine PDC address"
    exit 1
fi

# check that domain controller is alive
smbclient -N -L ${PDC} > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "Error in netlogon sync, could not open connection to DC"
    exit 1
fi

# get target directory
TARGETDIR="`echo '' | testparm --section-name netlogon 2> /dev/null | \
    sed -n 's/^[ \t]*path[ ]*=[ ]*\(.*\)$/\1/p'`"
if [ "${TARGETDIR}" = "" ]; then
    echo "Error in netlogon sync, failed to determine netlogon directory"
    exit 1
fi
if [ ! -d "${TARGETDIR}" ]; then
    echo "Error in netlogon sync, netlogon directory does not exist"
    exit 1
fi

# create temporary working directory
TMPDIR=/tmp/netlogon.$$
if [ -d ${TMPDIR} ]; then
    echo "Temporary directory ${TMPDIR} already exists."
    exit 1
fi
mkdir ${TMPDIR}

# copy netlogon data from domain controller
( smbclient //${PDC}/netlogon -N -Tqc - | (cd ${TMPDIR} ; tar xf - ) ) \
    >> /var/log/samba/log.sync-netlogon 2>&1
if [ $? -ne 0 ]; then
    echo "Error in netlogon sync see /var/log/samba/log.sync-netlogon"
    exit 1
fi

rsync -a --delete ${TMPDIR}/ ${TARGETDIR}/
if [ $? -ne 0 ]; then
    echo "Error in netlogon sync, rsync failed"
    exit 1
fi
chcon -Rh -u system_u -t samba_share_t ${TARGETDIR}/

rm -rf ${TMPDIR}
