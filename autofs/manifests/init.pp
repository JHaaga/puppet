
class autofs {

    package { "autofs":
        ensure => installed,
    }

    service { "autofs":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package["autofs"],
    }

    file { "/etc/auto.master":
        ensure  => present,
        source  => [
          "puppet:///files/autofs/auto.master.${::homename}",
          "puppet:///files/autofs/auto.master",
          "puppet:///modules/autofs/auto.master",
        ],
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["autofs"],
        notify  => Service["autofs"],
    }

}
