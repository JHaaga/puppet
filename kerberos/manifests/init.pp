
# Install and configure kerberos client
#
# === Global variables
#
#   $kerberos_realm:
#       Kerberos realm name.
#
#   $kerberos_kdc:
#       Array containing list of Kerberos KDC servers.
#
#   $kerberos_kadmin:
#       Kerberos admin server address. Defaults to first KDC server.
#
#   $kerberos_kpasswd:
#       Kerberos password change server address. Defaults to first
#       KDC server.
#
class kerberos::client {

    case $::operatingsystem {
        "centos","redhat","fedora": {
            package { "krb5-workstation":
                ensure => installed,
                before => File["krb5.conf"],
            }
        }
        "openbsd": {}
        "ubuntu": {
            package { [ "krb5-clients", "krb5-user", ]:
                ensure => installed,
                before => File["krb5.conf"],
            }
        }
        default: {
            fail("kerberos::client not supported in ${::operatingsystem}")
        }
    }

    file { "krb5.conf":
        ensure  => present,
        path    => $::operatingsystem ? {
            "openbsd" => "/etc/kerberosV/krb5.conf",
            default   => "/etc/krb5.conf",
        },
        content => template("kerberos/krb5.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

}


# Configure kerberos authentication
#
# === Global variables
#
#   $kerberos_realm:
#       Kerberos realm name.
#
#   $kerberos_kdc:
#       Array containing list of Kerberos KDC servers.
#
#   $kerberos_kadmin:
#       Kerberos admin server address. Defaults to first KDC server.
#
#   $kerberos_kpasswd:
#       Kerberos password change server address. Defaults to first
#       KDC server.
#
class kerberos::auth {

    include pam::common

    include kerberos::client
    $kdclist = inline_template('<%= @kerberos_kdc.join(" ") -%>')

    case $::operatingsystem {
        "centos","redhat","fedora": {
            package { "pam_krb5":
                ensure => installed,
            }
            exec { "authconfig --enablekrb5 --krb5kdc='${kdclist}' --krb5realm='${kerberos_realm}' --krb5adminserver='${kerberos_kadmin}' --update":
                path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                unless  => "egrep '^USEKERBEROS=yes\$' /etc/sysconfig/authconfig",
                before  => Class["kerberos::client"],
                require => Package["authconfig", "pam_krb5"],
            }
        }
        "ubuntu": {
            package { "libpam-krb5":
                ensure => installed,
            }
        }
        default: {
            fail("kerberos::auth not supported on ${::operatingsystem}")
        }
    }
}


# Install Kerberos server
#
# === Global variables
#
#   $kerberos_realm:
#       Kerberos realm name.
#
#   $kerberos_datadir:
#       Directory where to store Kerberos database files
#       defaults to /srv/kerberos
#
class kerberos::server {

    require kerberos::client

    package { "krb5-server":
        ensure => installed,
    }

    if $kerberos_datadir {
        file { $kerberos_datadir:
            ensure => directory,
            mode   => "0600",
            owner  => "root",
            group  => "root",
        }
        file { "/srv/kerberos":
            ensure  => link,
            target  => $kerberos_datadir,
            owner   => "root",
            group   => "root",
            require => File[$kerberos_datadir],
        }
    } else {
        file { "/srv/kerberos":
            ensure => directory,
            mode   => "0600",
            owner  => "root",
            group  => "root",
        }
    }

    file { "/var/kerberos/krb5kdc/kdc.conf":
        ensure  => present,
        content => template("kerberos/kdc.conf.erb"),
        mode    => "0600",
        owner   => "root",
        group   => "root",
        require => [ Package["krb5-server"], File["/srv/kerberos"], ],
        notify  => Service["krb5kdc"],
    }

    service { "krb5kdc":
        ensure    => running,
        enable    => true,
        subscribe => File["/etc/krb5.conf"],
    }

    file { "/var/kerberos/krb5kdc/kadm5.acl":
        ensure  => present,
        content => template("kerberos/kadm5.acl.erb"),
        mode    => "0600",
        owner   => "root",
        group   => "root",
        require => Package["krb5-server"],
        notify  => Service["kadmin"],
    }

    service { "kadmin":
        ensure  => running,
        enable  => true,
        require => Service["krb5kdc"],
    }

}


# Install Kerberos server with LDAP backend
#
# === Global variables
#
#   $kerberos_realm:
#       Kerberos realm name.
#
#   $kerberos_datadir:
#       Directory where to store Kerberos authentication keys
#       defaults to /srv/kerberos
#
class kerberos::server::ldap inherits kerberos::server {

    package { "krb5-server-ldap":
        ensure => installed,
        before => Service["krb5kdc"],
    }

    File["/var/kerberos/krb5kdc/kdc.conf"] {
        content => template("kerberos/kdc-ldap.conf.erb"),
    }

}


# Create keytab file.
#
# === Parameters
#
#   $name:
#       Keytab file path.
#   $principals:
#       List of principals to be added into keytab
#   $ensure:
#       Set to present to create keytab and absent to remove it
#   $owner:
#       Owner for keytab file
#   $group:
#       Group for keytab file
#   $mode:
#       Permissions for keytab file
#
# === Sample usage
#
# kerberos::keytab { "/etc/krb5.keytab":
#     ensure     => present,
#     principals => [ "host/testhost.foo.sh@FOO.SH" ],
# }
#
define kerberos::keytab($principals = [], $ensure = present, $owner = "root", $group = "", $mode = "0600") {

    case $group {
        "": {
            case $::operatingsystem {
                "openbsd": { $real_group = "wheel" }
                default:   { $real_group = "root" }
            }
        }
        default: {
            $real_group = $group
        }
    }

    file { $name:
        ensure  => $ensure,
        content => template("kerberos/keytab.erb"),
        mode    => $mode,
        owner   => $owner,
        group   => $real_group,
    }

}
