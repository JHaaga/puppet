
# Install inetd server.
#
# This class is wrapper for installing inetd superserver.
#
class inetd::server {

    case $::operatingsystem {
        "centos","redhat","fedora","ubuntu","debian": {
            include inetd::server::xinetd
        }
        "openbsd": {
            include inetd::server::inetd
        }
        default: {
            fail("Inetd module not supported in ${::operatingsystem}")
        }
    }

}


# Install xinetd server.
#
# This class should not be invoked directly. Instead use
# inetd::server which installs xinetd or normal inetd
# depending on running operatingsystem.
#
class inetd::server::xinetd {

    package { "xinetd":
        ensure => installed,
    }

    service { "xinetd":
        ensure  => running,
        enable  => true,
        require => Package["xinetd"],
    }

}

# Install inetd server.
#
# This class should not be invoked directly. Instead use
# inetd::server which installs xinetd or normal inetd
# depending on running operatingsystem.
#
class inetd::server::inetd {

    service { "inetd":
        ensure  => running,
        start   => "/usr/sbin/inetd",
        enable  => true,
    }

}


# Configure inetd service.
#
# === Parameters
#
#   $name:
#       Service name
#   $ensure:
#       Set to present to enable service and absent to disable.
#
# === Sample usage
#
# inetd::service { "time-stream":
#     ensure => present,
# }
#
define inetd::service($ensure = present) {

    include inetd::server

    case $::operatingsystem {
        "centos","redhat","ubuntu","debian": {
            service { $name:
                enable => $ensure ? {
                    present => true,
                    absent  => false,
                },
                notify => Service["xinetd"],
            }
        }
        "fedora": {
            augeas { "config-inetd-${name}":
                context => "/files/etc/xinetd.d/${name}/service",
                changes => $ensure ? {
                    "present" => "set disable no",
                    "absent"  => "set disable yes",
                },
                notify  => Service["xinetd"],
            }
        }
        "openbsd": {
            exec { "enable-inetd-${name}":
                command => $ensure ? {
                    "present" => "ruby -pi -e 'sub(/^#${name}(\\s+)/, \"${name}\\\\1\")' /etc/inetd.conf",
                    "absent"  => "ruby -pi -e 'sub(/^${name}(\\s+)/, \"#${name}\\\\1\")' /etc/inetd.conf",
                },
                unless  => $ensure ? {
                    "present" => "egrep '^${name}[[:space:]]' /etc/inetd.conf",
                    "absent"  => "egrep '^#${name}[[:space:]]' /etc/inetd.conf",
                },
                path    => "/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
                notify  => Service["inetd"],
            }
        }
        default: {
            fail("Inetd module not supported in ${::operatingsystem}")
        }
    }

}
