# Install Apache Solr.
#
class solr {

    if !$solr_package {
        if $solr_package_latest {
            $solr_package = $solr_package_latest
        } else {
            fail("Must define \$solr_package or \$solr_package_latest")
        }
    }

    if !$solr_core {
        fail("Must define \$solr_core")
    }

    file { "/usr/local/src/solr.tgz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///files/packages/${solr_package}"
    }

    util::extract::tar { "/usr/local/share/solr":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/solr.tgz",
        require => File['/usr/local/src/solr.tgz'],
    }

    include user::system
    realize(User["solr"], Group["solr"])

    if $solr_datadir {
        file { $solr_datadir:
            ensure  => directory,
            mode    => "0770",
            owner   => "solr",
            group   => "solr",
            require => User["solr"],
        }

        file { "/srv/solr":
            ensure  => link,
            target  => $solr_datadir,
            require => File[$solr_datadir],
        }
    } else {
        file { "/srv/solr":
            ensure  => directory,
            mode    => "0770",
            owner   => "solr",
            group   => "solr",
            require => User["solr"],
        }
    }

    file { [ "/srv/solr/cores",
             "/srv/solr/cores/lib",
             "/srv/solr/index",
             "/srv/solr/run",
             "/srv/solr/run/solr-webapp",
             "/srv/solr/spool", ]:
        ensure  => directory,
        mode    => "0770",
        owner   => "solr",
        group   => "solr",
        require => File["/srv/solr"],
    }

    file { "/srv/solr/cores/solr.xml":
        ensure  => present,
        mode    => "0660",
        owner   => "solr",
        group   => "solr",
        content => template("solr/solr.xml.erb"),
        require => File["/srv/solr/cores"],
    }

    file { "/srv/solr/run/start.jar":
        ensure  => link,
        target  => "/usr/local/share/solr/example/start.jar",
        require => File["/srv/solr/run"],
    }
    file { "/srv/solr/run/contexts":
        ensure  => link,
        target  => "/usr/local/share/solr/example/contexts",
        require => File["/srv/solr/run"],
    }
    file { "/srv/solr/run/etc":
        ensure  => link,
        target  => "/usr/local/share/solr/example/etc",
        require => File["/srv/solr/run"],
    }
    file { "/srv/solr/run/lib":
        ensure  => link,
        target  => "/usr/local/share/solr/example/lib",
        require => File["/srv/solr/run"],
    }
    file { "/srv/solr/run/webapps":
        ensure  => link,
        target  => "/usr/local/share/solr/example/webapps",
        require => File["/srv/solr/run"],
    }

    file { "/etc/init.d/solr":
        ensure => present,
        mode   => "0755",
        owner  => "root",
        group  => "root",
        source => "puppet:///modules/solr/solr.init",
        notify => Exec["add-service-solr"],
    }
    exec { "add-service-solr":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => $::operatingsystem ? {
            "debian" => "update-rc.d solr defaults",
            "ubuntu" => "update-rc.d solr defaults",
            default  => "chkconfig --add solr",
        },
        refreshonly => true,
        before      => Service["solr"],
    }

    service { "solr":
        ensure => running,
        enable => true,
    }

    file { "/etc/solr":
        ensure => directory,
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }
    file { "/etc/solr/htpasswd":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => $apache::sslserver::group,
        require => File["/etc/solr"],
    }

    $htdocs = "/usr/local/share/solr/htdocs"

    file { $htdocs:
        ensure => directory,
        mode   => "0755",
        owner  => "root",
        group  => "root",
        require => Util::Extract::Tar["/usr/local/share/solr"],
    }
    file { "${htdocs}/.htaccess":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [ "puppet:///files/solr/htaccess",
                     "puppet:///modules/solr/htaccess", ],
        require => File[$htdocs],
    }

    define configwebhost($htdocs) {
        file { "/srv/www/https/${name}/solr":
            ensure  => link,
            target  => $htdocs,
            require => File["/srv/www/https/${name}"],
        }
    }

    if $solr_webhosts {
        apache::configfile { "solr.conf":
            http   => false,
            source => "puppet:///modules/solr/solr-httpd.conf",
        }

        configwebhost { $solr_webhosts:
            htdocs => $htdocs,
        }
    }

    if !$pysolr_package {
        if $pysolr_package_latest {
            $pysolr_package = $pysolr_package_latest
        } else {
            fail("Must define \$pysolr_package or \$pysolr_package_latest")
        }
    }

    python::setup::install { "/usr/local/src/pysolr":
        source => "puppet:///files/packages/${pysolr_package}",
    }

}
