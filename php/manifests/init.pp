
# Install PHP command-line interface
#
class php::cli {

    package { "php-cli":
        ensure => installed,
    }

}


# Install GD support to PHP
#
class php::gd {

    package { "php-gd":
        ensure => installed,
    }

}


# Install MySQL support to PHP
#
class php::mysql {

    package { "php-mysql":
        ensure => installed,
    }

}


# Install PostgreSQL support to PHP
#
class php::pgsql {

    package { "php-pgsql":
        ensure => installed,
    }

}


# Install PDO database abstraction support to PHP
#
class php::pdo {

    package { "php-pdo":
        ensure => installed,
    }

}

