Facter.add('amanda_serverkey') do
    setcode do
        begin
            match = File.read('/var/lib/amanda/.ssh/id_rsa_amdump.pub')[/^ssh-rsa ([^ ]+)/, 1]
            if match
                data = match
            end
        rescue
            data = ''
        end
        data
    end
end

Facter.add('amanda_clientkey') do
    setcode do
        begin
            match = File.read('/var/lib/amanda/.ssh/id_rsa_amrecover.pub')[/^ssh-rsa ([^ ]+)/, 1]
            if match
                data = match
            end
        rescue
            data = ''
        end
        data
    end
end
