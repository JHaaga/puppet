# Set GNOME gconf values.
#
# === Parameters
#
#   $name:
#       Key to update
#   $value:
#       Value for given key
#   $source:
#       Source to update. Valid values are "default" and "mandatory",
#       defaults to "default".
#   $type:
#       Value type. Valid values are "string" and "int", defaults to
#       string.
#
define gnome::gconf($value, $source = "default", $type = "string", $ltype = "") {

    case $source {
        "mandatory": {
            $xml = "/etc/gconf/gconf.xml.mandatory"
        }
        "default": {
            $xml = "/etc/gconf/gconf.xml.defaults"
        }
        default: {
            fail("Invalid gnome::gconf source.")
        }
    }

    case $ltype {
        "string": {
            $param = "--list-type 'string'"
        }

        default: {
            $param = ""
        }
    }

    exec { "gconftool-2 --direct --config-source xml:readwrite:${xml} --type ${type} $param --set '${name}' '${value}'":
        path   => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless => "test \"`gconftool-2 --direct --config-source xml:readwrite:${xml} --get '${name}'`\" = '${value}'",
    }

}
