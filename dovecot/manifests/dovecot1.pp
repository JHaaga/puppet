class dovecot::server::v1 {
    case $operatingsystem {
        centos,fedora: {
            $dovecot_ssl_dir = "/etc/pki/tls"
        }
        default: {
            fail("Dovecot module not supported in ${operatingsystem}.")
        }
    }

    service { "dovecot":
        ensure  => running,
        enable  => true,
        require => File["/etc/dovecot.conf"],
    }

    if $dovecot_ssl_csr {
        file { "$dovecot_ssl_dir/private/dovecot.csr":
            ensure => present,
            source => $dovecot_ssl_csr,
            mode   => "0640",
            owner  => "root",
            group  => "root",
            notify => Service["dovecot"],
        }
    }

    if $dovecot_ssl_ca {
        file { "$dovecot_ssl_dir/certs/dovecot.ca.crt":
            ensure => present,
            source => $dovecot_ssl_ca,
            mode   => "0644",
            owner  => "root",
            group  => "root",
            notify => Service["dovecot"],
        }
    }

    if $dovecot_ssl_cert {
        file { "$dovecot_ssl_dir/certs/dovecot.crt":
            ensure => present,
            source => $dovecot_ssl_cert,
            mode   => "0644",
            owner  => "root",
            group  => "root",
            notify => Service["dovecot"],
        }
    } else {
        fail("You need to define an ssl_cert in your node manifest.")
    }

    if $dovecot_ssl_key {
        file { "$dovecot_ssl_dir/private/dovecot.key":
            ensure => present,
            source => $dovecot_ssl_key,
            mode   => "0600",
            owner  => "root",
            group  => "root",
            notify => Service["dovecot"],
        }
    } else {
        fail("You need to define an ssl_key in your node manifest.")
    }

    file { "/etc/dovecot.conf":
        ensure  => present,
        content => template("dovecot/dovecot.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        notify  => Service["dovecot"],
    }
}
