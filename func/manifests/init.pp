
class func::minion {

    package { "func":
        ensure => installed,
    }

    file { "/etc/func/minion.conf":
        ensure  => present,
        source  => [ "puppet:///files/func/minion.conf",
                     "puppet:///modules/func/minion.conf", ],
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["func"],
        notify  => Service["funcd"],
    }

    file { "/etc/pki/certmaster/${hostname}.pem":
        ensure  => present,
        source  => "${puppet_ssldir}/private_keys/${fqdn}.pem",
        mode    => "0600",
        owner   => "root",
        group   => "root",
        require => Package["func"],
        notify  => Service["funcd"],
    }
    file { "/etc/pki/certmaster/${hostname}.cert":
        ensure  => present,
        source  => "${puppet_ssldir}/certs/${fqdn}.pem",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["func"],
        notify  => Service["funcd"],
    }
    file { "/etc/pki/certmaster/ca.cert":
        ensure  => present,
        source  => "${puppet_ssldir}/certs/ca.pem",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["func"],
        notify  => Service["funcd"],
    }

    service { "funcd":
        ensure => running,
        enable => true,
    }

}


class func::server inherits func::minion {

    file { "/etc/pki/certmaster/ca":
        ensure => directory,
        mode   => "0750",
        owner  => "root",
        group  => "sysadm",
    }
    exec { "umask 077; openssl rsa -in ${puppet_ssldir}/ca/ca_key.pem -out /etc/pki/certmaster/ca/certmaster.key -passin file:${puppet_ssldir}/ca/private/ca.pass":
        path    => "/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
        creates => "/etc/pki/certmaster/ca/certmaster.key",
        require => File["/etc/pki/certmaster/ca"],
    }
    file { "/etc/pki/certmaster/ca/certmaster.crt":
        ensure  => present,
        source  => "${puppet_ssldir}/ca/ca_crt.pem",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => File["/etc/pki/certmaster/ca"],
    }

    file { "/var/lib/certmaster/certmaster":
        ensure => directory,
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }
    file { "/var/lib/certmaster/certmaster/certs":
        ensure => link,
        target => "${puppet_ssldir}/ca/signed",
    }

    file { "/etc/certmaster/certmaster.conf":
        ensure  => present,
        source  => [ "puppet:///files/func/certmaster.conf",
                    "puppet:///modules/func/certmaster.conf", ],
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["func"],
    }

}
