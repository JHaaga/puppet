
# Install and configure ifstated.
#
class ifstated {

    file { "/etc/ifstated.conf":
        ensure => present,
        source => [ "puppet:///private/ifstated.conf",
                    "puppet:///files/firewall/ifstated.conf.${fqdn}",
                    "puppet:///files/firewall/ifstated.conf", ],
        mode   => "0644",
        owner  => "root",
        group  => "wheel",
        notify => Service["ifstated"],
    }

    service { "ifstated":
        ensure => running,
        enable => true,
        binary => "ifstated",
        start  => "/usr/sbin/ifstated",
    }

}
