# Install libvirt software.
#
class libvirt::client {

    include user::system
    realize(User["qemu"], Group["qemu"])

    file { "/var/lib/qemu":
        ensure  => directory,
        mode    => "0700",
        owner   => "qemu",
        group   => "qemu",
        require => [ User["qemu"], Group["qemu"], ],
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            package { [ "libvirt", "virt-manager", "virt-viewer", ]:
                ensure  => installed,
                require => File["/var/lib/qemu"],
            }
        }
        default: {
            fail("Not supported on ${::operatingsystem}.")
        }
    }

}


# Install and configure KVM and libvirtd.
#
# === Global variables
#
#   $libvirt_admingroup:
#       Group which has access to system libvirtd.
#
#   $libvirt_guest_on_boot
#       Action to taken on host boot [start, ignore] (default: start)
#
#   $libvirt_guest_on_shutdown
#       Action to taken on host shutdown [suspend, shutdown] (default: suspend)
#
#   $libvirt_parallel_shutdown
#       If set to non-zero, shutdown will suspend guests concurrently. (default: 0)
#
class libvirt::kvm inherits libvirt::client {

    if !$libvirt_admingroup {
        $libvirt_admingroup = "root"
    }

    if !$libvirt_guest_on_boot {
        $libvirt_guest_on_boot = "start"
    }

    if !$libvirt_guest_on_shutdown {
        $libvirt_guest_on_shutdown = "suspend"
    }

    if !$libvirt_parallel_shutdown {
        $libvirt_parallel_shutdown = 0
    }

    case $::operatingsystem {
        "centos","redhat": {
            case $::operatingsystemrelease {
                /^5\./: {
                    package { ["kvm", "kmod-kvm"]:
                        ensure  => installed,
                        before  => Service["libvirtd"],
                        require => [ User["qemu"], Group["qemu"] ],
                    }
                }
                default: {
                    package { "qemu-kvm":
                        ensure  => installed,
                        before  => Service["libvirtd"],
                        require => [ User["qemu"], Group["qemu"] ],
                    }
                    package { "ruby-libvirt":
                        ensure => installed,
                    }
                }
            }
            file { "/etc/sysconfig/libvirt-guests":
                ensure  => present,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                content => template("libvirt/sysconfig-libvirt-guests.erb"),
                require => Package["libvirt"],
            }
        }
        "fedora": {
            package { "qemu-kvm":
                ensure  => installed,
                before  => Service["libvirtd"],
                require => [ User["qemu"], Group["qemu"] ],
            }
            package { "ruby-libvirt":
                ensure => installed,
            }
        }
        default: {
            fail("Operating system not supported")
        }
    }

    file { "/etc/libvirt/libvirtd.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("libvirt/libvirtd.conf.erb"),
        require => Package["libvirt"],
        notify  => Service["libvirtd"],
    }

    service { "libvirtd":
        ensure => running,
        enable => true,
    }
}

