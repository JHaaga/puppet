
# Install mirroring scripts
#
# === Parameters
#
#   $datadir:
#       Directory for mirrored data. Defaults to /srv/mirrors
#
class mirror($datadir = "/srv/mirrors") {

    include user::system
    realize(User["mirror"], Group["mirror"])

    if $datadir != "/srv/mirrors" {
        file { "/srv/mirrors":
            ensure  => link,
            target  => $datadir,
            owner   => "root",
            group   => "root",
            seltype => "httpd_sys_content_t",
            before  => File[$datadir],
        }
        selinux::manage_fcontext { "/srv/mirrors(/.*)?":
            type   => "httpd_sys_content_t",
            before => File["/srv/mirrors"],
        }
    }
    file { $datadir:
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        seltype => "httpd_sys_content_t",
    }
    selinux::manage_fcontext { "${datadir}(/.*)?":
        type   => "httpd_sys_content_t",
        before => File[$datadir],
    }

    file { "/etc/sync-mirrors":
        ensure  => directory,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        recurse => true,
        purge   => true,
    }

    file { [ "/var/run/sync-mirrors", "/var/log/sync-mirrors", ]:
        ensure  => directory,
        mode    => "0755",
        owner   => "mirror",
        group   => "mirror",
        before  => Cron["sync-mirrors"],
        require => User["mirror"],
    }

    file { "/usr/local/bin/sync-mirrors":
        ensure => present,
        source => "puppet:///modules/mirror/sync-mirrors",
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }
    cron { "sync-mirrors":
        command => "/usr/local/bin/sync-mirrors",
        user    => "mirror",
        hour    => [ 0, 6, 12, 18, ],
        minute  => 0,
        require => [ File["/usr/local/bin/sync-mirrors"], User["mirror"], ],
    }

}


# Create new mirror
#
# === Parameters:
#
#   $name:
#       Mirror name
#
#   $source:
#       Rsync path from where to sync mirror
#
#   $rsync_options:
#       Extra options for rsync
#
# === Sample usage:
#
# mirrors::mirror { "centos":
#     source => "rsync://rsync.nic.funet.fi/ftp/pub/mirrors/centos.org/",
#     rsync_options => [
#       "--exclude=SRPMS",
#       "--exclude=debug",
#       "--exclude=isos",
#     ],
# }
#
define mirror::mirror($source, $rsync_options=[]) {

    require mirror

    file { "/etc/sync-mirrors/${name}.conf":
        ensure  => present,
        content => template("mirror/mirror.conf.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => File["/srv/mirrors/${name}"],
    }

    file { "/srv/mirrors/${name}":
        ensure  => directory,
        owner   => "mirror",
        group   => "mirror",
        seltype => "httpd_sys_content_t",
    }

}


# Run weekly hardlinking for mirrored data
#
class mirror::hardlink {

    require mirror

    package { "hardlink":
        ensure => installed,
    }
    cron { "hardlink-mirrors-weekly":
        command => "/usr/sbin/hardlink /srv/mirrors/",
        user    => "mirror",
        hour    => 4,
        minute  => 0,
        weekday => 0,
    }

}


# Install Fedora mirror reporting tool
#
# https://fedoraproject.org/wiki/Infrastructure/Mirroring
#
# === Parameters:
#
#   $sitename:
#       Site name configured to mirrormanager
#
#   $password:
#       Site password configured to mirrormanager
#
#   $hostname:
#       Host name configured to mirrormanager
#
#   $mirrors:
#       Components mirrored to this hosts
#
# === Sample usage:
#
# mirrors::reportmirror {
#     sitename => "foo.sh"
#     password => "secret",
#     hostname => "mirrors.foo.sh",
#     mirrors  => [ "fedora", "epel", ],
# }
#
class mirror::reportmirror($sitename, $password, $hostname, $mirrors=[]) {

    package { "mirrormanager-client":
        ensure => installed,
    }

    file { "/etc/mirrormanager-client/report_mirror.conf":
        ensure  => present,
        content => template("mirror/report_mirror.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "mirror",
        require => [ Package["mirrormanager-client"], Group["mirror"], ],
    }

}
