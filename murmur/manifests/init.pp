# Install murmur (Mumble server).
#
# === Global variables
#
#   $murmur_ssl_key:
#       Path to SSL private key.
#
#   $murmur_ssl_cert:
#       Path to SSL certificate.
#
#   $murmur_password:
#       Server passwords. Defaults to none.
#
#   $murmur_welcome:
#       Server welcome message.
#
class murmur {

    if !$murmur_package {
        if $murmur_package_latest {
            $murmur_package = $murmur_package_latest
        } else {
            fail("Must define \$murmur_package or \$murmur_package_latest")
        }
    }

    if !$murmur_password {
        $murmur_password = ""
    }
    if !$murmur_welcome {
        $murmur_welcome = "<br />Welcome to this server running Murmur.<br />"
    }

    file { "/usr/local/src/murmur-static_x86.tar.bz2":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///files/packages/${murmur_package}",
    }
    util::extract::tar { "/usr/local/murmur":
        strip   => 1,
        source  => "/usr/local/src/murmur-static_x86.tar.bz2",
        require => File["/usr/local/src/murmur-static_x86.tar.bz2"],
        notify  => Service["murmur"],
    }

    include user::system
    realize(User["murmur"], Group["murmur"])

    if !$murmur_ssl_key {
        $murmur_ssl_key = "${puppet_ssldir}/private_keys/${homename}.pem"
    }
    if !$murmur_ssl_cert {
        $murmur_ssl_cert = "${puppet_ssldir}/certs/${homename}.pem"
    }

    include ssl
    file { "${ssl::certs}/murmur.crt":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => $murmur_ssl_cert,
        notify => Service["murmur"],
    }
    file { "${ssl::private}/murmur.key":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "murmur",
        source  => $murmur_ssl_key,
        require => Group["murmur"],
        notify  => Service["murmur"],
    }

    file { "/srv/murmur":
        ensure  => directory,
        mode    => "0700",
        owner   => "murmur",
        group   => "murmur",
        require => User["murmur"],
    }
    file { "/srv/murmur/murmur.ini":
        ensure  => present,
        mode    => "0600",
        owner   => "murmur",
        group   => "murmur",
        content => template("murmur/murmur.ini.erb"),
        require => File["/srv/murmur"],
        notify  => Service["murmur"],
    }

    file { "/etc/init.d/murmur":
        ensure => present,
        mode   => "0755",
        owner  => "root",
        group  => "root",
        source => "puppet:///modules/murmur/murmur.init",
        notify => Exec["add-service-murmur"],
    }
    exec { "add-service-murmur":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "chkconfig --add murmur",
        refreshonly => true,
        before      => Service["murmur"],
    }
    service { "murmur":
        ensure => running,
        enable => true,
    }

}
