
# Install procmail
#
class procmail {

    package { "procmail":
        ensure => present,
    }

    file { "/etc/procmailrc.d":
        ensure  => directory,
        purge   => true,
        force   => true,
        recurse => true,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        source  => "puppet:///modules/custom/empty",
        require => Package["procmail"],
        notify  => Exec["generate-procmailrc"],
    }

    file { "/etc/procmailrc":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["procmail"],
    }

    exec { "generate-procmailrc":
        command     => '/bin/sh -c -- \'umask 022; ( echo "DROPPRIVS=yes" ; find /etc/procmailrc.d/*.rc -exec echo "INCLUDERC={}" \; ) > /etc/procmailrc ; true\'',
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
    }

}

# Add config file to procmail
#
# === Parameters
#
#   $name:
#       Config file name.
#   $source:
#       Config file source. Defaults to "puppet:///files/procmail/$name".
#
# === Sample usage
#
# procmail::rc { "spamassassin.rc":
#     source => "/etc/mail/spamassassin/spamassassin-spamc.rc",
# }
#
define procmail::rc($source = "AUTO") {

    file { "/etc/procmailrc.d/${name}":
        ensure => present,
        source => $source ? {
            "AUTO"  => "puppet:///files/procmail/${name}",
            default => $source,
        },
        mode   => "0644",
        owner  => root,
        group  => root,
        notify => Exec["generate-procmailrc"],
    }

}
