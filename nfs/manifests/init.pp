
# Install NFS client.
#
class nfs::client {

    require portmap::server

    package { "nfs-utils":
        ensure => installed,
        name   => $::operatingsystem ? {
            "ubuntu" => "nfs-common",
            default  => "nfs-utils",
        },
    }

    if $::operatingsystem == "Fedora" and $::operatingsystemrelease >= 16 {
        # https://bugzilla.redhat.com/show_bug.cgi?id=692008
        service { "NetworkManager-wait-online":
            ensure => stopped,
            enable => true,
        }
    }

    service { "nfslock":
        name      => $::operatingsystem ? {
            "fedora" => $::operatingsystemrelease ? {
                /^([1-9]|1[0-5])$/ => "nfslock",
                default            => "nfs-lock",
            },
            "ubuntu" => "statd",
            default  => "nfslock",
        },
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package["nfs-utils"],
    }

}


# Install and configure NFS server.
#
class nfs::server {

    require nfs::client

    file { "/etc/exports":
        ensure  => present,
        source  => [
          "puppet:///files/nfs/exports.${::fqdn}",
          "puppet:///modules/nfs/exports",
        ],
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["nfs-utils"],
        notify  => Exec["exportfs"],
    }

    file { "/etc/sysconfig/nfs":
        ensure  => present,
        content => template("nfs/nfs.sysconfig.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        notify  => Service["nfs"],
    }

    service { "nfs":
        ensure    => running,
        name      => $::operatingsystem ? {
            "fedora" => $::operatingsystemrelease ? {
                /^([1-9]|1[0-5])$/ => "nfs",
                default            => "nfs-server",
            },
            default  => "nfs",
        },
        enable    => true,
        hasstatus => true,
        require   => Service["nfslock"],
    }

    exec { "exportfs":
        command     => "exportfs -rav",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        require     => Service["nfs"],
    }

}
