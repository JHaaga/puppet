
# Install OpenOffice
#
class openoffice {

    package { [ "openoffice.org-calc",
                "openoffice.org-draw",
                "openoffice.org-impress",
                "openoffice.org-writer", ]:
        ensure => installed,
    }

}
