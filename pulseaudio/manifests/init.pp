
# Install common pulseaudio components
#
class pulseaudio::common {

    package { [ "pulseaudio", "pulseaudio-module-zeroconf" ]:
        ensure => installed,
    }

}


# Install system wide pulseaudio daemon.
#
# Before using, please see some warnings in:
#
#   http://pulseaudio.org/wiki/WhatIsWrongWithSystemMode
#
class pulseaudio::server inherits pulseaudio::common {

    file { "/etc/init.d/pulseaudio":
        ensure => present,
        source => "puppet:///modules/pulseaudio/pulseaudio.init",
        mode   => "0755",
        owner  => root,
        group  => root,
    }
    file { "/etc/sysconfig/pulseaudio":
        ensure => present,
        source => "puppet:///modules/pulseaudio/pulseaudio.sysconfig.${::operatingsystem}",
        mode   => "0644",
        owner  => root,
        group  => root,
        before => File["/etc/init.d/pulseaudio"],
        notify => Service["pulseaudio"],
    }

    file { "/etc/pulse/system.pa":
        ensure  => present,
        source  => [ "puppet:///files/pulseaudio/system.pa.${fqdn}",
                     "puppet:///files/pulseaudio/system.pa", ],
        mode    => "0644",
        owner   => root,
        group   => root,
        require => Package["pulseaudio"],
        notify  => Service["pulseaudio"],
    }

    service { "pulseaudio":
        ensure  => running,
        enable  => true,
        require => [ Package["pulseaudio"],
                     File["/etc/init.d/pulseaudio"], ],
    }

    user { "pulse":
        groups  => [ "audio", ],
        home    => "/var/run/pulse",
        require => Package["pulseaudio"],
        before  => Service["pulseaudio"],
    }

}
