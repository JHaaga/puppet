# Install cairo.
#
class cairo {

    package { "cairo":
        name   => $::operatingsystem ? {
            debian  => "libcairo2",
            ubuntu  => "libcairo2",
            default => "cairo",
        },
        ensure => installed,
    }

}


# Install python bindings for cairo.
#
class cairo::python inherits cairo {

    package { "pycairo":
        name   => $::operatingsystem ? {
            debian  => "python-cairo",
            ubuntu  => "python-cairo",
            default => "pycairo",
        },
        ensure => installed,
    }

}
