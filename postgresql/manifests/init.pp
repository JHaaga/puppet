
# Install PostgreSQL client
#
class postgresql::client {

    package { "postgresql":
        ensure => installed,
    }

}


# Install PostgreSQL server
#
# === Parameters:
#
#   $datadir:
#       Data directory for databases.
#
class postgresql::server($datadir="/srv/pgsql") {

    case $::operatingsystem {
        "centos","redhat": {
            file { "/etc/sysconfig/pgsql/postgresql":
                ensure  => present,
                content => "PGDATA=/srv/pgsql\n",
                mode    => "0644",
                owner   => "root",
                group   => "root",
                require => Package["postgresql-server"],
                notify  => Service["postgresql"],
            }
        }
        default: {
            fail("postgresql::server not supported in ${::operatingsystem}")
        }
    }

    exec { "service postgresql initdb":
        user    => "root",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        creates => "/srv/pgsql/postgresql.conf",
        require => File["/srv/pgsql"],
        before  => Service["postgresql"],
    }

    if $datadir != "/srv/pgsql" {
        file { $datadir:
            ensure  => directory,
            mode    => "0700",
            owner   => "postgres",
            group   => "postgres",
            seltype => "postgresql_db_t",
            require => Package["postgresql"],
        }
        file { "/srv/pgsql":
            ensure  => link,
            target  => $datadir,
            owner   => "root",
            group   => "root",
            seltype => "postgresql_db_t",
            require => File[$datadir],
        }
        selinux::manage_fcontext { "${datadir}(/.*)?":
            type   => "postgresql_db_t",
            before => File[$datadir],
        }
    } else {
        file { "/srv/pgsql":
            ensure  => directory,
            mode    => "0700",
            owner   => "postgres",
            group   => "postgres",
            seltype => "postgresql_db_t",
            require => Package["postgresql"],
        }
    }
    selinux::manage_fcontext { "/srv/pgsql(/.*)?":
        type   => "postgresql_db_t",
        before => File["/srv/pgsql"],
    }

    package { "postgresql-server":
        ensure => installed,
    }

    service { "postgresql":
        ensure    => running,
        enable    => true,
        hasstatus => true,
    }

}
