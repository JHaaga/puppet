# Edit xdg autostart settings.
#
# === Parameters
#
#   $name:
#       Service name.
#   $enable:
#       Whether to start service, true or false.
#
define xdg::autostart($enable) {

    exec { "xdg-autostart-set-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        cwd     => "/etc/xdg/autostart",
        command => "echo 'X-GNOME-Autostart-enabled=${enable}' >> ${name}.desktop",
        unless  => "grep -q 'X-GNOME-Autostart-enabled' ${name}.desktop",
        before  => Exec["xdg-autostart-sub-${name}"],
    }
    exec { "xdg-autostart-sub-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        cwd     => "/etc/xdg/autostart",
        command => "ruby -pi -e 'sub(/^(X-GNOME-Autostart-enabled).*/, \"\\\1=${enable}\")' ${name}.desktop",
        unless  => "grep -q 'X-GNOME-Autostart-enabled=${enable}' ${name}.desktop",
    }

}
