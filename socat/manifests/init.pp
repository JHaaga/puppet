
# Install socat
class socat::package {

    package { "socat":
        ensure => installed,
    }

}
